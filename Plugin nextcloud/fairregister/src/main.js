/**
 SPDX-FileCopyrightText: fairkom <philipp.monz@fairkom.eu>
 SPDX-License-Identifier: AGPL-3.0-or-later
    -->
 */

import App from "./App";
import UserService from "../js/UserService";
import Vue from "vue";
import { generateFilePath } from "@nextcloud/router";
// eslint-disable-next-line
__webpack_public_path__ = generateFilePath(appName, "", "js/");

export default UserService.initKeycloak(async () => {
	new Vue({
		el: "#content",
		render: (h) => h(App),
	});
});




