OCA.Files.fileActions.registerAction({
	name: "fairregister",
	displayName: t("fairregister", "Add to fairregister"),
	mime: "file",
	permissions: OC.PERMISSION_READ,
	iconClass: "icon-filetype-file",
	actionHandler: (name, context) => {
		// Get the file ID and filename from the context object
		var fileId = context.fileInfoModel.attributes.id;
		var filename = context.fileInfoModel.attributes.name;

		// Construct the URL with the file ID and filename
		var url = OC.generateUrl("apps/fairregister") + "?fileId=" + fileId + "&filename=" + filename;

		// Redirect to the constructed URL
		window.location.href = url;
	},
});
