import Keycloak from "keycloak-js";

const _kc = new Keycloak({
  url: "https://id.fairkom.net/auth",
  clientId: "fairregister-development",
  realm: "fairlogin",
  "ssl-requiered": "external",
  "public-client": true,
});

/**
 * Initializes Keycloak instance and calls the provided callback function if successfully authenticated.
 *
 * @param onAuthenticatedCallback
 */
const initKeycloak = (onAuthenticatedCallback) => {
  _kc
    .init({
      onLoad: "check-sso",
      //silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html',
      pkceMethod: "S256",
    })
    .then((authenticated) => {
      if (!authenticated) {
        console.log("user not authenticated");
        doLogin();
      }
      if (authenticated) {
        console.log("user is authenticated");
      }
      onAuthenticatedCallback();
    })
    .catch((error) => {
      console.error(error);
    });
};

const doLogin = _kc.login;

const doRegister = _kc.register;

const doLogout = _kc.logout;

const getToken = () => {
  return _kc.token;
};

const isLoggedIn = () => {
  return !!_kc.token;
};

const updateToken = (successCallback) =>
  _kc.updateToken(5).then(successCallback).catch(doLogin);

const getUserId = () => _kc.tokenParsed?.sub;
const getUsername = () => _kc.tokenParsed?.preferred_username;
const getFirstName = () => _kc.tokenParsed?.given_name;
const getLastName = () => _kc.tokenParsed?.family_name;
const getDisplayName = () => _kc.tokenParsed?.name;
const getEMail = () => _kc.tokenParsed?.email;
const getRoleAccessList = () => _kc.getUserRoles();

const hasRole = (roles) => roles.some((role) => _kc.hasRealmRole(role));

const UserService = {
  initKeycloak,
  getUserId,
  doLogin,
  doRegister,
  doLogout,
  isLoggedIn,
  getToken,
  updateToken,
  getUsername,
  getFirstName,
  getLastName,
  getDisplayName,
  getEMail,
  getRoleAccessList,
  hasRole,
};

export default UserService;
