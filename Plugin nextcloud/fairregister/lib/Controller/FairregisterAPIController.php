<?php
// SPDX-FileCopyrightText: fairkom <philipp.monz@fairkom.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\Fairregister\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\Response;
use OC\Http\Client\ClientService;

use Exception;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataDisplayResponse;
use OCP\AppFramework\Http\RedirectResponse;
use OCP\Files;
use OCP\Files\IRootFolder;
use OCP\Files\NotPermittedException;
use OCP\IConfig;
use OCP\IRequest;
use OCP\Http\Client\IClientService;
use Psr\Log\LoggerInterface;
use OCP\IUserSession;
use OCP\Files\ObjectStore\IObjectStore;
use OCA\Fairregister\Service\FairregisterAPIService;
use OCA\Fairregister\AppInfo\Application;

class FairregisterAPIController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var IConfig
     */
    private $config;
    /**
     * @var IRootFolder
     */
    private IRootFolder $storage;
    /**
     * @var \OCP\Http\Client\IClient
     */
    private $client;
    /**
     * @var IUserSession
     */
    private $userSession;

    public function __construct(
        string $appName,
        IClientService $clientService,
        LoggerInterface $logger,
        IRequest $request,
        IConfig $config,
        IRootFolder $storage,
        IUserSession $userSession
    ) {
        parent::__construct($appName, $request);
        $this->config = $config;
        $this->logger = $logger;
        $this->nextcloudUserId = $this->storage = $storage;
        $this->client = $clientService->newClient();
        $this->userSession = $userSession;
        $this->objectStore = $objectStore;
        $this->fairregisterAPIService = $fairregisterAPIService;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @param string $filename
     * @param string $userId
     * @param string $keycloakToken
     * @param string $fileId
     * @return DataResponse
     */
    public function sendFile(
        string $filename,
        string $userId,
        string $token,
        string $fileId
    ): DataResponse {
        if (empty($filename) || empty($userId) || empty($token)) {
            return new DataResponse(
                ["error" => "Invalid parameters"],
                Http::STATUS_BAD_REQUEST
            );
        }

        try {
            $url = "http://49.12.218.124:8080/api-service/files/upload/presigned-url?filename={$filename}&userId={$userId}";
            $options = [
                "headers" => [
                    "Content-Type" => "application/json",
                    "Authorization" => "Bearer " . $token,
                    "Host" => "49.12.218.124:8080",
                ],
            ];

            $response = $this->client->get($url, $options);
            $statusCode = $response->getStatusCode();
            $this->logger->warning("statusCode");
            $this->logger->warning(print_r($statusCode, true));
            $body = $response->getBody();
            $this->logger->warning("body");
            $this->logger->warning(print_r($body, true));
            
            if ($statusCode >= 400 && $statusCode < 600) {
                return $this->createErrorResponse($statusCode);
            } else {
				$result = json_decode($body);
				$prefix = $result->prefix;
                $url2 = $result->url;
				$filename2 = $result->filename;

                $this->logger->warning(print_r($url2, true));
                $this->logger->warning("fileID");
                $this->logger->warning(print_r($fileId, true));
                $intfileId = intval($fileId);

                // Get the currently logged-in user
                $user = $this->userSession->getUser();
                // Get the user's ID
                $userNextCloudId = $user->getUID();
                // Get the user's home folder
                $userFolder = $this->storage->getUserFolder($userNextCloudId);
                // Get the file
                $files = $userFolder->getById($fileId);
                $file = $files[0];
				// Get the file resource
				$ressource = $file->fopen('r');

				$this->logger->warning("ressource");
                $this->logger->warning(print_r($ressource, true));

				//$this->logger->warning("files all");
                //$this->logger->warning(print_r($file, true));
				$options2 = [
					"body" => $ressource,
				];
			
				$response = $this->client->put($url2, $options2);
			
                $statusCode = $response->getStatusCode();

                $this->logger->warning("statusCode");
                $this->logger->warning(print_r($statusCode, true));

                $this->logger->warning("response");
                $this->logger->warning(print_r($response, true));
				$body = $response->getBody();
                if ($statusCode >= 400 && $statusCode < 600) {
                    return $this->createErrorResponse($statusCode);
                } else {
                    // Erfolgreiche Antwort
                    return new DataResponse($prefix, Http::STATUS_OK);
                }
            }
        } catch (ServerException | ClientException $e) {
            $errorMessage =
                "Fairregister API send file error: " . $e->getMessage();
            return new DataResponse(
                ["error" => $errorMessage],
                Http::STATUS_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Erstellt eine DataResponse für einen Fehler basierend auf dem Statuscode.
     *
     * @param int $statusCode Der HTTP-Statuscode
     * @return DataResponse Die erstellte DataResponse für den Fehler
     */
    private function createErrorResponse($statusCode): DataResponse {
        $errorMessages = [
            401 => "Unauthorized",
            403 => "Forbidden",
            404 => "Not Found",
        ];

        if (isset($errorMessages[$statusCode])) {
            $errorMessage = $errorMessages[$statusCode];
        } elseif ($statusCode >= 400 && $statusCode < 500) {
            $errorMessage = "Client Error";
        } elseif ($statusCode >= 500 && $statusCode < 600) {
            $errorMessage = "Server Error";
        } else {
            $errorMessage = "Unknown Error";
        }

        return new DataResponse(["error" => $errorMessage], $statusCode);
    }
}
