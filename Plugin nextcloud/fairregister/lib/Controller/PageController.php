<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: fairkom <philipp.monz@fairkom.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\Fairregister\Controller;

use OCA\Fairregister\AppInfo\Application;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IRequest;
use OCP\Util;

class PageController extends Controller {
	public function __construct(IRequest $request) {
		parent::__construct(Application::APP_ID, $request);
	}

 	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
 	*/
	public function index(): TemplateResponse {
		Util::addScript(Application::APP_ID, 'fairregister-main');
		Util::addScript(Application::APP_ID, 'main');

		return new TemplateResponse(Application::APP_ID, 'main');
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
 	public function fairregister(): TemplateResponse {
		Util::addScript(Application::APP_ID, 'fairregister-main');
		Util::addScript(Application::APP_ID, 'main');

		return new TemplateResponse(Application::APP_ID, 'main');
	} 
}
