FROM node:18 as builder
WORKDIR /app
COPY Frontend/*.json ./
RUN npm install
COPY Frontend ./
RUN npm run build

FROM nginx:alpine
COPY deployments/local/nginx-fairregister.conf /etc/nginx/nginx.conf
COPY --from=builder /app/build /usr/share/nginx/html