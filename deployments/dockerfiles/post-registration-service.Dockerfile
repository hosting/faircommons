FROM maven:3-sapmachine-21 as builder
WORKDIR /app
COPY ./faircommons-services .
RUN mvn clean package

FROM sapmachine:21-jre-headless-ubuntu
WORKDIR /app
COPY --from=builder /app/post-registration-service/target/post-registration-service*.jar ./post-registration-service.jar
ENTRYPOINT ["java","-jar","./post-registration-service.jar"]