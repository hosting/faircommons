FROM maven:3-sapmachine-21 as builder
WORKDIR /app
COPY ./faircommons-services .
RUN mvn clean package

FROM sapmachine:21-jre-headless-ubuntu
# Install SSH client
RUN apt-get update && apt-get install -y openssh-client && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY ./deployments/scripts/setup-ssh-tunnel.sh ./setup-ssh-tunnel.sh
RUN chmod +x ./setup-ssh-tunnel.sh

COPY --from=builder /app/ipfs-service/target/ipfs-service*.jar ./ipfs-service.jar
ENTRYPOINT ["java","-jar","./ipfs-service.jar"]