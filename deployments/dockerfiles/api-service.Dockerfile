FROM maven:3-sapmachine-21 as builder
WORKDIR /app
COPY ./faircommons-services .
RUN mvn clean package

FROM sapmachine:21-jre-headless-ubuntu
WORKDIR /app
COPY --from=builder /app/api-service/target/api-service*.jar ./api-service.jar
ENTRYPOINT ["java","-jar","./api-service.jar"]