FROM maven:3-sapmachine-21 as builder
WORKDIR /app
COPY ./faircommons-services .
RUN mvn clean package

FROM sapmachine:21-jre-headless-ubuntu
WORKDIR /app
COPY --from=builder /app/hashing-service/target/hashing-service*.jar ./hashing-service.jar
ENTRYPOINT ["java","-jar","./hashing-service.jar"]