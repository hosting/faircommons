# faircommons

This is the code repository of the faircommons project. It is the successor of RegisteredCommons.org and has been deployed as fairregister.net by fairkom.eu. The development has been funded by the Austrian netidee fund.       


## Local Deployment
1. Requires [Docker](https://docs.docker.com/engine/install/) with [Compose](https://github.com/docker/compose/releases/latest)
2. Start only project containers (postgres, minio, ipfs) using command `docker-compose -f ./deployments/local/docker-compose.yml up -d`
3. Or you can start project and frontend containers using command `docker-compose -f ./deployments/local/docker-compose.yml -f ./deployments/local/docker-compose.frontend.yml up -d --build`
3. Or you can start project and backend containers using command `docker-compose -f ./deployments/local/docker-compose.yml -f ./deployments/local/docker-compose.backend.yml up -d --build`
4. Or you can start the complete project (project, frontend and backend containers) using command `docker-compose -f ./deployments/local/docker-compose.yml -f ./deployments/local/docker-compose.frontend.yml -f ./deployments/local/docker-compose.backend.yml up -d --build `

Edit `/etc/hosts` file with `sudo vi /etc/hosts` and add `127.0.0.1 nginx-minio`

API Endpoints:
http://localhost:8080/api-service/swagger-ui/index.html

### Useful commands
| Command | Discription
|-|-
| `docker-compose -f ./deployments/local/docker-compose.yml down -v` | Stops containers and removes containers, networks, volumes, and images created by up.
| `docker-compose -f ./deployments/local/docker-compose.yml logs` | Shows logs of containers (use flag `-f` to follow logs)
| `docker system prune -a -f` | Remove all unused containers, networks, images (flag `--volumes` prune volumes)
|  `docker-compose -f ./deployments/local/docker-compose.yml stop` | Stops running containers without removing them


## Documentation

- [Project Overview](https://git.fairkom.net/hosting/faircommons/-/wikis/home)       
- [User Documentation](user-documentation)     
- [Developer Documentation](developer-documentation)     
