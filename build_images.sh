#!/bin/bash

# Array of Dockerfile locations and their respective image names
dockerfiles=("deployments/dockerfiles/api-service.Dockerfile" 
             "deployments/dockerfiles/hashing-service.Dockerfile"
             "deployments/dockerfiles/ipfs-service.Dockerfile"
             "deployments/dockerfiles/post-registration-service.Dockerfile"
             "deployments/dockerfiles/frontend-fairregister.Dockerfile")

imagetags=("registry.osalliance.com/faircommons/api-service"
           "registry.osalliance.com/faircommons/hashing-service"
           "registry.osalliance.com/faircommons/ipfs-service"
           "registry.osalliance.com/faircommons/post-registration-service"
           "registry.osalliance.com/faircommons/frontend-fairregister")

# Check if version is provided as argument
if [ -z "$1" ]; then
    echo "Please provide version as an argument."
    exit 1
fi

version=$1

# Loop over the array and build each image with the provided version tag
for index in ${!dockerfiles[*]}; do
    echo "Building ${imagetags[$index]} with tag $version"
    
    docker buildx build -f "${dockerfiles[$index]}" --platform linux/amd64 -t "${imagetags[$index]}:$version" -t "${imagetags[$index]}:latest" . --push
done
