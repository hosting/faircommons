import React from "react";
import MyWorksPage from "../MyWorksPage";
import SideMenu from "./SideMenu";

const Works = () => {
  return (
    <div className="flex">
      <SideMenu></SideMenu>
      <section className="bg-white w-screen dark:bg-gray-900">
        <MyWorksPage></MyWorksPage>
      </section>
    </div>
  );
};

export default Works;
