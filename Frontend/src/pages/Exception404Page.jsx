import React from "react";
import EasyTrans from "../components/EasyTrans";

const Exception404Page = () => {
  return (
    <section className="bg-white h-screen flex">
      <div className="py-8 px-4 mx-auto max-w-screen-xl  lg:py-16 lg:px-6">
        <div className="mx-auto max-w-screen-sm text-center ">
          <h1 className="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-primary-600 dark:text-primary-500">
            <EasyTrans>ErrorHandling.404.CodeDescription</EasyTrans>
          </h1>
          <p className="mb-4 text-3xl tracking-tight font-bold text-gray-900 md:text-4xl dark:text-white">
            <EasyTrans>ErrorHandling.404.Header</EasyTrans>
          </p>
          <p className="mb-4 text-lg font-light text-gray-500 dark:text-gray-400">
            <EasyTrans>ErrorHandling.404.Text</EasyTrans>
          </p>
          <div className="flex justify-around">
            <button className="basis-1/2 p-3">
              <a
                href="#features"
                className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-green-600 hover:bg-green-700 md:py-4 md:text-lg md:px-10"
              >
                <EasyTrans>ErrorHandling.404.standardButton</EasyTrans>
              </a>
            </button>
            <button className="basis-1/2 p-3">
              <a
                href="#features"
                className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-green-600 hover:bg-green-700 md:py-4 md:text-lg md:px-10"
              >
                <EasyTrans>ErrorHandling.404.secondButton</EasyTrans>
              </a>
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Exception404Page;
