const formatBytesSizeUnits = (bytes) => {
  if (bytes >= 1073741824) {
    bytes = (bytes / 1073741824).toFixed(2) + " GB";
    return bytes;
  } else if (bytes >= 1048576) {
    bytes = (bytes / 1048576).toFixed(2) + " MB";
    return bytes;
  } else if (bytes >= 1024) {
    bytes = (bytes / 1024).toFixed(2) + " KB";
    return bytes;
  } else if (bytes > 1) {
    bytes = bytes + " bytes";
    return bytes;
  } else if (bytes === 1) {
    bytes = bytes + " byte";
    return bytes;
  } else {
    bytes = "0 bytes";
    return bytes;
  }
};

const FormatService = {
  formatBytesSizeUnits,
};

export default FormatService;
