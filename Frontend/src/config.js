const REACT_APP_API =
  window.REACT_APP_API || "https://api.dev.fairregister.net/api-service";
const KEYCLOAK_CLIENT_ID =
  window.KEYCLOAK_CLIENT_ID || "fairregister-development";

export { REACT_APP_API, KEYCLOAK_CLIENT_ID };
