module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./node_modules/flowbite/**/*.js"],
  theme: {
    extend: {},
    colors: {
      transparent: "transparent",
      current: "currentColor",
      fairblack: "#00000",
      fairwhite: "#ffffff",
      fairgray: "#fafafa",
      fairbrightgreen: "#c2d115",
      fairdarkgreen: "#82ad29",
      fairbrightblue: "#5365CA",
      fairblue: {
        100: "#D1D7E9",
        200: "#A3AED5",
        300: "#7485C0",
        400: "#465BAC",
        500: "#283583",
        600: "#1F2A6A",
        700: "#171E51",
        800: "#0E1238",
        900: "#05061F",
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};
