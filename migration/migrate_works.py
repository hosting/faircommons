import requests
import psycopg
import os
import mimetypes

from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from keycloak import KeycloakAdmin
from keycloak import KeycloakOpenIDConnection

keycloak_connection = KeycloakOpenIDConnection(
    server_url="https://id.fairkom.net/auth/",
    username="",
    password="",
    realm_name="",
    user_realm_name="",
    client_id="admin-cli",
    verify=True,
)

keycloak_admin = KeycloakAdmin(connection=keycloak_connection)

# API endpoint to obtain access token
token_url = "https://id.fairkom.net/auth/realms/fairlogin/protocol/openid-connect/token"

# Client ID and secret
client_id = ""
client_secret = ""

# Base fairregister API url
base_api_url = ""


# create an OAuth2Session instance
client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)

# fetch an access token using the client credentials flow
token = oauth.fetch_token(
    token_url=token_url, client_id=client_id, client_secret=client_secret
)

# create a requests.Session instance and set the access token as a header
session = requests.Session()
session.headers["Authorization"] = f"Bearer {token['access_token']}"


def upload_work_file(url, file_path):
    with open(file_path, "rb") as f:
        filename = os.path.basename(file_path)
        file_contents = f.read()
        headers = {'Content-Type': mimetypes.guess_type(filename)[0]}
        response = requests.put(url, data=file_contents, headers=headers)

        if response.status_code != 200:
            print("Failed to upload_work_file with id: " + str(work[0]))


def create_presigned_upload_url(userId, filename):
    response = session.get(
        f"{base_api_url}/files/upload/presigned-url?userId={userId}&filename={filename}"
    )

    if response.status_code != 200:
        print("Failed to create_presigned_upload_url with id: " + str(work[0]))

    return response.json()


def get_language_code(number):
    codes = {
        1: "en",  # English
        2: "de",  # German (Deutsch)
        3: "fr",  # French (Français)
        4: "it",  # Italian (Italiano)
        5: "es",  # Spanish (Español)
        6: "nl",  # Dutch (Nederlands)
        7: "pl",  # Polish (Polski)
    }

    return codes.get(number, "")


def get_visability(number):
    visability = {0: "PRIVATE", 1: "PUBLIC"}

    return visability.get(number, "PUBLIC")


def create_work_data(work, userId, presigned_upload_url, author):
    license = get_license(work[15])
    description = work[5];

    if license['add_url_to_description']:
        description += " --- Licence notice: " + work[15]
    
    work_data = {
        "type": get_type(work[37]),
        "title": work[1],
        "description": description,
        "tags": [x.strip() for x in work[3].split(',')],
        "language": get_language_code(work[22]),
        "creationDate": work[23].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        if work[23]
        else None,
        "registrar": userId,
        "creator": {
            "author": {
                # "id": str(uuid.uuid4()),
                "firstName": author[2],
                "lastName": author[3],
                "email": author[1],
                "pseudonym": author[4],
                "url": author[5],
            },
            "percentage": 100,
        },
        "contributors": [],
        "publisher": work[4],
        "identifier": work[8],
        "workFile": {
            "prefix": presigned_upload_url['prefix'],
            "filename": presigned_upload_url['filename']
        },
        "visibility": get_visability(work[26]),
        "storageOptions": ["FAIRREGISTER_FILE_SYSTEM"],
        "grid": work[12],
        "license": {
            "identifier": license['identifier'],
            "addons": work[18],
            "dual": "",
            "dalicc": "",
        },
        "registrationDate": work[24].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    }

    return work_data


def get_license(url):
    license_dict = {
        "http://creativecommons.org/licenses/by/2.5/it/": {
            "identifier": "CC-BY-2.5-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/4.0/": {
            "identifier": "CC-BY-ND-4.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc/3.0/": {
            "identifier": "CC-BY-NC-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by/3.0/us/": {
            "identifier": "CC-BY-3.0-US",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-sa/2.0/at/": {
            "identifier": "CC-BY-SA-2.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.0/fr/": {
            "identifier": "CC-BY-NC-ND-2.0-FR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-sa/3.0/it/": {
            "identifier": "CC-BY-SA-3.0-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-sa/4.0/": {
            "identifier": "CC-BY-SA-4.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by/2.0/uk/": {
            "identifier": "CC-BY-2.0-UK",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/3.0/de/": {
            "identifier": "CC-BY-NC-ND-3.0-DE",
            "add_url_to_description": False,
        },
        "http://www.gnu.org/licenses/gpl-2.0.html": {
            "identifier": "GPL-2.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc/3.0/at/": {
            "identifier": "CC-BY-NC-3.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.0/de/": {
            "identifier": "CC-BY-NC-ND-2.0-DE",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/br/": {
            "identifier": "CC-BY-NC-SA-2.5-BR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/2.5/": {
            "identifier": "CC-BY-ND-2.5",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-sa/2.5/it/": {
            "identifier": "CC-BY-SA-2.5-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/co/": {
            "identifier": "CC-BY-NC-ND-2.5-CO",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/3.0/": {
            "identifier": "CC-BY-ND-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-sa/2.0/de/": {
            "identifier": "CC-BY-SA-2.0-DE",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/2.5/ar/": {
            "identifier": "CC-BY-ND-2.5-AR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/3.0/at/": {
            "identifier": "CC-BY-ND-3.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/hr/": {
            "identifier": "CC-BY-NC-SA-2.5-HR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.5/pt/": {
            "identifier": "CC-BY-NC-2.5-PT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/es/": {
            "identifier": "CC-BY-NC-ND-2.5-ES",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/3.0/cl/": {
            "identifier": "CC-BY-NC-ND-3.0-CL",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/publicdomain/zero/1.0/": {
            "identifier": "CC0-1.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by/2.5/hr/": {
            "identifier": "CC-BY-2.5-HR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by/4.0/": {
            "identifier": "CC-BY-4.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/au/": {
            "identifier": "CC-BY-NC-ND-2.5-AU",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/publicdomain/": {
            "identifier": "Copyright-Only Dedication",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.0/at/": {
            "identifier": "CC-BY-NC-2.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/ca/": {
            "identifier": "CC-BY-NC-ND-2.5-CA",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/au/": {
            "identifier": "CC-BY-NC-SA-2.5-AU",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.5/": {
            "identifier": "CC-BY-NC-2.5",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.0/de/": {
            "identifier": "CC-BY-NC-SA-2.0-DE",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by/2.5/scotland/": {
            "identifier": "CC-BY-2.5-SCOTLAND",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.0/kr/": {
            "identifier": "CC-BY-NC-ND-2.0-KR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/ca/": {
            "identifier": "CC-BY-NC-SA-2.5-CA",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/3.0/it/": {
            "identifier": "CC-BY-NC-ND-3.0-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/3.0/de/": {
            "identifier": "CC-BY-NC-3.0-DE",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by/2.0/de/": {
            "identifier": "CC-BY-2.0-DE",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.0/de/": {
            "identifier": "CC-BY-NC-2.0-DE",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/pt/": {
            "identifier": "CC-BY-NC-ND-2.5-PT",
            "add_url_to_description": True,
        },
        "http://en.wikipedia.org/wiki/Copyright": {
            "identifier": "Full Copyright",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/3.0/at/": {
            "identifier": "CC-BY-NC-ND-3.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/3.0/": {
            "identifier": "CC-BY-NC-ND-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nd/2.5/br/": {
            "identifier": "CC-BY-ND-2.5-BR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by/3.0/": {
            "identifier": "CC-BY-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-sa/2.5/": {
            "identifier": "CC-BY-SA-2.5",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/3.0/it/": {
            "identifier": "CC-BY-NC-SA-3.0-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-sa/3.0/at/": {
            "identifier": "CC-BY-SA-3.0-AT",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-sa/3.0/": {
            "identifier": "CC-BY-SA-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-sa/3.0/gt/": {
            "identifier": "CC-BY-SA-3.0-GT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.5/it/": {
            "identifier": "CC-BY-NC-2.5-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/2.0/de/": {
            "identifier": "CC-BY-ND-2.0-DE",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by/2.0/at/": {
            "identifier": "CC-BY-2.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/pt/": {
            "identifier": "CC-BY-NC-SA-2.5-PT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/3.0/": {
            "identifier": "CC-BY-NC-SA-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/": {
            "identifier": "CC-BY-NC-SA-2.5",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/in/": {
            "identifier": "CC-BY-NC-SA-2.5-IN",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/2.5/pt/": {
            "identifier": "CC-BY-ND-2.5-PT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/sampling+/1.0/": {
            "identifier": "CC-SAMPLING+-1.0",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/sampling/1.0/": {
            "identifier": "CC-SAMPLING-1.0",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.5/ar/": {
            "identifier": "CC-BY-NC-2.5-AR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/br/": {
            "identifier": "CC-BY-NC-ND-2.5-BR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/GPL/2.0/": {
            "identifier": "CC-GPL-2.0",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/2.0/fr/": {
            "identifier": "CC-BY-NC-2.0-FR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by/2.5/br/": {
            "identifier": "CC-BY-2.5-BR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-sa/2.1/jp/": {
            "identifier": "CC-BY-SA-2.1-JP",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.0/at/": {
            "identifier": "CC-BY-NC-ND-2.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/it/": {
            "identifier": "CC-BY-NC-SA-2.5-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc/3.0/us/": {
            "identifier": "CC-BY-NC-3.0-US",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/4.0/": {
            "identifier": "CC-BY-NC-SA-4.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc/4.0/": {
            "identifier": "CC-BY-NC-4.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/3.0/at/": {
            "identifier": "CC-BY-NC-SA-3.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by/3.0/at/": {
            "identifier": "CC-BY-3.0-AT",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/3.0/de/": {
            "identifier": "CC-BY-NC-SA-3.0-DE",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nd/2.5/in/": {
            "identifier": "CC-BY-ND-2.5-IN",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/3.0/us/": {
            "identifier": "CC-BY-NC-SA-3.0-US",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/4.0/": {
            "identifier": "CC-BY-NC-ND-4.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/ch/": {
            "identifier": "CC-BY-NC-SA-2.5-CH",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/3.0/us/": {
            "identifier": "CC-BY-ND-3.0-US",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/": {
            "identifier": "CC-BY-NC-ND-2.5",
            "add_url_to_description": False,
        },
        "http://www.gnu.org/licenses/gpl-3.0.html": {
            "identifier": "GPL-3.0",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/it/": {
            "identifier": "CC-BY-NC-ND-2.5-IT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.5/ar/": {
            "identifier": "CC-BY-NC-SA-2.5-AR",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/mt/": {
            "identifier": "CC-BY-NC-ND-2.5-MT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nd/2.0/at/": {
            "identifier": "CC-BY-ND-2.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-sa/3.0/us/": {
            "identifier": "CC-BY-SA-3.0-US",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/3.0/es/": {
            "identifier": "CC-BY-NC-ND-3.0-ES",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.5/ch/": {
            "identifier": "CC-BY-NC-ND-2.5-CH",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/nc-sampling+/1.0/": {
            "identifier": "CC-NC-SAMPLING+-1.0",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-sa/2.0/at/": {
            "identifier": "CC-BY-NC-SA-2.0-AT",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.1/jp/": {
            "identifier": "CC-BY-NC-ND-2.1-JP",
            "add_url_to_description": True,
        },
        "http://creativecommons.org/licenses/by/2.5/": {
            "identifier": "CC-BY-2.5",
            "add_url_to_description": False,
        },
        "http://creativecommons.org/licenses/by-nc-nd/2.0/cl/": {
            "identifier": "CC-BY-NC-ND-2.0-CL",
            "add_url_to_description": True,
        },
    }

    return license_dict[url]


def register_work(work_data):
    response = session.post(
        f"{base_api_url}/works/register",
        json=work_data
    )

    if response.status_code != 201:
        print("Failed to register_work with id: " + str(work[0]))

def get_type(type):
    typ_dict = {
        "Document": "DOCUMENT",
        "Audio": "AUDIO",
        "Image": "PICTURE",
        "Video": "VIDEO",
        "Software": "SOFTWARE",
        "Interactive": "INTERACTIVE"
    }

    return typ_dict[type]

# Connect to an existing database
with psycopg.connect(
    dbname="registeredcommons",
    user="admin",
    password="password",
    host="localhost",
    port="5431",
) as conn:
    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        # get all authors which registered a work
        cur.execute(
            """
            SELECT distinct authors_id, authors_email, authors_firstname, authors_surname, authors_nickname, authors_url
            FROM authors
            JOIN works ON authors.authors_id = works.works_registrar_ref
            """
        )
        for author in cur.fetchall():
            # get all works for this author
            print(
                "+++++++++++++++++ Start migration for "
                + author[1]
                + " +++++++++++++++++"
            )
            cur.execute(
                """
                SELECT * FROM works
                INNER JOIN filetypes ON works.works_type_ref = filetypes.filetypes_id
                WHERE works.works_registrar_ref = %(author_id)s
                """,
                {"author_id": author[0]},
            )

            for work in cur.fetchall():
                if work[14]: #if no filename ignore it
                    user_id_keycloak = keycloak_admin.get_user_id(author[1])
                    presigned_upload_url = create_presigned_upload_url(user_id_keycloak, work[17])
                    work_data = create_work_data(work, user_id_keycloak, presigned_upload_url, author)
                    upload_work_file(presigned_upload_url['url'], "works/" + work[14])
                    register_work(work_data)
                else:
                    print("No Filename found for work with id: " + str(work[0]))