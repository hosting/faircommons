import json
import psycopg
import secrets
import string
import time
from keycloak import KeycloakAdmin
from keycloak import KeycloakOpenIDConnection

alphabet = string.ascii_letters + string.digits

keycloak_connection = KeycloakOpenIDConnection(
    server_url="https://id.fairkom.net/auth/",
    username="",
    password="",
    realm_name="",
    user_realm_name="",
    client_id="admin-cli",
    verify=True,
)

keycloak_admin = KeycloakAdmin(connection=keycloak_connection)
created_users = []

# Connect to an existing database
with psycopg.connect(
    dbname="registeredcommons",
    user="admin",
    password="password",
    host="localhost",
    port="5431",
) as conn:
    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        # get all authors which registered a work
        cur.execute(
            """
            SELECT distinct authors_id, authors_email, authors_firstname, authors_surname, authors_nickname, authors_url
            FROM authors
            JOIN works ON authors.authors_id = works.works_registrar_ref
            """
        )
        for author in cur.fetchall():
            time.sleep(2)
            user_id = keycloak_admin.get_user_id(author[1])
            if user_id == None:
                password = "".join(secrets.choice(alphabet) for i in range(12))
                user_id = keycloak_admin.create_user(
                    {
                        "email": author[1],
                        "username": author[1],
                        "enabled": True,
                        "firstName": author[2],
                        "lastName": author[3],
                        "credentials": [{"value": password, "type": "password"}],
                        "requiredActions": ["UPDATE_PASSWORD"],
                        "attributes": {
                            "nickname":  author[4],
                        },
                    }
                )
                print("Created new user")
                created_users.append(
                    {
                        "userId": user_id,
                        "email": author[1],
                        "password": password,
                    }
                )

with open("created_users.json", "w") as file_object:
    json.dump(created_users, file_object)
