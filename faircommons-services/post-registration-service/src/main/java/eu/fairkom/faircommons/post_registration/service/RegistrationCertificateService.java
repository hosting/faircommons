package eu.fairkom.faircommons.post_registration.service;

import com.itextpdf.html2pdf.HtmlConverter;
import eu.fairkom.faircommons.common.models.views.WorkView;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class RegistrationCertificateService {
    private final SpringTemplateEngine templateEngine;

    public RegistrationCertificateService(SpringTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public InputStream generateCertificate(WorkView work) {
        try (var os = new ByteArrayOutputStream()) {
            var context = new Context();
            context.setVariable("work", work);

            var html = templateEngine.process("registration-certificate_template", context);
            HtmlConverter.convertToPdf(html, os);

            return new ByteArrayInputStream(os.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
