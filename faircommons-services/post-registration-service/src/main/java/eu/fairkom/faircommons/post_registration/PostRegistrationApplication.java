package eu.fairkom.faircommons.post_registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostRegistrationApplication {
    public static void main(String[] args) {
        SpringApplication.run(PostRegistrationApplication.class, args);
    }
}
