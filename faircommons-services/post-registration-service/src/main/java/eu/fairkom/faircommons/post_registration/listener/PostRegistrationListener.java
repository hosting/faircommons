package eu.fairkom.faircommons.post_registration.listener;


import eu.fairkom.faircommons.common.rabbitmq.RabbitMqListener;
import eu.fairkom.faircommons.common.rabbitmq.messages.PostRegistrationMessage;
import eu.fairkom.faircommons.post_registration.service.PostRegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.POST_REGISTRATION_QUEUE;

@Component
@RabbitListener(queues = POST_REGISTRATION_QUEUE)
public class PostRegistrationListener implements RabbitMqListener<PostRegistrationMessage> {
    private static final Logger logger = LoggerFactory.getLogger(PostRegistrationListener.class);
    private final PostRegistrationService postRegistrationService;

    public PostRegistrationListener(PostRegistrationService postRegistrationService) {
        this.postRegistrationService = postRegistrationService;
    }

    @Override
    public void receive(PostRegistrationMessage message) {
        logger.info("Received message for work ID: {}", message.work().id());
        var work = message.work();

        if (postRegistrationService.isOwnStorageOptionPresent(work.storageOptions())) {
            postRegistrationService.removeWorkFile(work);
        }

        postRegistrationService.generateRegistrationCertificate(work);
    }
}
