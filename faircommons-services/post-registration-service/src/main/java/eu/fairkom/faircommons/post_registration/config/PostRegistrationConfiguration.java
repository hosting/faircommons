package eu.fairkom.faircommons.post_registration.config;

import eu.fairkom.faircommons.common.minio.MinioConfiguration;
import eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration;
import eu.fairkom.faircommons.common.security.SecurityWebAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackageClasses = {RabbitMqConfiguration.class, MinioConfiguration.class})
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@Import(value = {SecurityWebAutoConfiguration.class})
public class PostRegistrationConfiguration {
}
