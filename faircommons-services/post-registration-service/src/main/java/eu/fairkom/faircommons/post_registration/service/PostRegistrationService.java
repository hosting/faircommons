package eu.fairkom.faircommons.post_registration.service;

import eu.fairkom.faircommons.common.minio.MinioService;
import eu.fairkom.faircommons.common.models.StorageOption;
import eu.fairkom.faircommons.common.models.views.WorkView;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PostRegistrationService {
    private final MinioService minioService;
    private final RegistrationCertificateService registrationCertificateService;

    public PostRegistrationService(MinioService minioService,
                                   RegistrationCertificateService registrationCertificateService) {
        this.minioService = minioService;
        this.registrationCertificateService = registrationCertificateService;
    }

    public void removeWorkFile(WorkView work) {
        minioService.removeFile(work.registrar(), work.workFile().filenameWithPrefix());
    }

    public boolean isOwnStorageOptionPresent(Set<StorageOption> storageOptions) {
        return storageOptions.stream()
                .anyMatch(option -> option.equals(StorageOption.OWN_STORAGE));
    }

    public void generateRegistrationCertificate(WorkView work) {
        var certificate = registrationCertificateService.generateCertificate(work);
        minioService.uploadRegistrationCertificate(work.registrar(), work.workFile().prefix(), certificate);
    }
}
