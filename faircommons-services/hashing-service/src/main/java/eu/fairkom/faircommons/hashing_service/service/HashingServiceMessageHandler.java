package eu.fairkom.faircommons.hashing_service.service;

import eu.fairkom.faircommons.common.minio.MinioService;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.views.HashCodeView;
import eu.fairkom.faircommons.common.rabbitmq.MessageHandler;
import eu.fairkom.faircommons.common.rabbitmq.messages.MetaDataMessage;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

@Service
public class HashingServiceMessageHandler extends MessageHandler {
    private static final Logger logger = LoggerFactory.getLogger(HashingServiceMessageHandler.class);
    private final HashingService hashingService;
    private final MinioService minioService;
    private final String algorithm;

    public HashingServiceMessageHandler(HashingService hashingService,
                                        MinioService minioService,
                                        RabbitTemplate rabbitTemplate,
                                        @Value("${hashing.algorithm}") String algorithm) {
        super(rabbitTemplate);
        this.hashingService = hashingService;
        this.minioService = minioService;
        this.algorithm = algorithm;
    }

    @Override
    public void handleWorkFileMessage(WorkFileMessage message) {
        var userId = message.userId();
        var filename = message.workFile().filenameWithPrefix();

        try (var workFileStream = minioService.downloadFile(userId, filename)) {

            var hashValue = hashingService.getChecksum(workFileStream, algorithm);
            var hashCode = new HashCodeView(algorithm, hashValue);

            logger.info("Work File hashing done. Sending success event for work ID {}", message.workId());
            sendStatusEvent(message.workId(), hashCode,
                    WorkStatus.WorkStatusType.FILE_HASHING_DONE);
        } catch (IOException e) {
            logger.error("Error while processing work file message with work ID {}: {}", message.workId(),
                    e.getMessage(), e);
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void handleMetaDataMessage(MetaDataMessage message) {
        try (var metaDataStream = new ByteArrayInputStream(message.metaData().getBytes(StandardCharsets.UTF_8))) {

            var hashValue = hashingService.getChecksum(metaDataStream, algorithm);
            var hashCode = new HashCodeView(algorithm, hashValue);

            logger.info("Meta Data hashing done. Sending success event for work ID {}", message.workId());
            sendStatusEvent(message.workId(),
                    hashCode, WorkStatus.WorkStatusType.META_HASHING_DONE);
        } catch (IOException e) {
            logger.error("Error while processing meta data message with work ID {}: {}", message.workId(),
                    e.getMessage(), e);
            throw new UncheckedIOException(e);
        }
    }
}
