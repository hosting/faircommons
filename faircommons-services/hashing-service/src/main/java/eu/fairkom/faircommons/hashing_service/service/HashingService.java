package eu.fairkom.faircommons.hashing_service.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class HashingService {

    public String getChecksum(InputStream inputStream, String algorithm) throws IOException {
        try {
            var messageDigest = MessageDigest.getInstance(algorithm);

            try (var digestInputStream = new DigestInputStream(inputStream, messageDigest);) {
                while (digestInputStream.read() != -1) ; //empty loop to clear the data
                messageDigest = digestInputStream.getMessageDigest();
            }

            return bytesToHex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
