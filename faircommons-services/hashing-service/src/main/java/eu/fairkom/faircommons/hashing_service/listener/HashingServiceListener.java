package eu.fairkom.faircommons.hashing_service.listener;

import eu.fairkom.faircommons.common.minio.MinioException;
import eu.fairkom.faircommons.common.rabbitmq.RabbitMqListener;
import eu.fairkom.faircommons.common.rabbitmq.messages.Message;
import eu.fairkom.faircommons.common.rabbitmq.messages.MetaDataMessage;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import eu.fairkom.faircommons.hashing_service.service.HashingServiceMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.HASHING_QUEUE;

@Component
@RabbitListener(queues = HASHING_QUEUE)
public class HashingServiceListener implements RabbitMqListener<Message> {
    private static final Logger logger = LoggerFactory.getLogger(HashingServiceListener.class);
    private final HashingServiceMessageHandler messageHandler;

    public HashingServiceListener(HashingServiceMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    @Override
    public void receive(Message message) {
        try {
            logger.info("Received message of type: {} for work ID: {}", message.type(), message.workId());
            switch (message.type()) {
                case FILE -> messageHandler.handleWorkFileMessage((WorkFileMessage) message);
                case META -> messageHandler.handleMetaDataMessage((MetaDataMessage) message);
                default -> logger.error("Unknown message type.");
            }
        } catch (MinioException e) {
            logger.error("Could not download work file for work ID: {}", message.workId(), e);
            messageHandler.handleError(message.workId(), e);
        } catch (Throwable e) {
            logger.error("Unexpected error during hashing for work ID: {}", message.workId(), e);
            messageHandler.handleError(message.workId(), e);
        }
    }
}
