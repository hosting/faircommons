package eu.fairkom.faircommons.ipfs_service.service;

import eu.fairkom.faircommons.ipfs_service.models.IpfsAddView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.util.Objects;

@Service
@ConditionalOnProperty(prefix = "ipfs", name = "service", havingValue = "cluster")
public class IpfsClusterService implements IpfsService {
    private static final Logger logger = LoggerFactory.getLogger(IpfsClusterService.class);
    private static final String UPLOAD_ENDPOINT = "/add?local=true&pin=true";
    private final RestTemplate restTemplate;

    public IpfsClusterService(RestTemplateBuilder restTemplateBuilder,
                              @Value("${ipfs.url}") String baseUrl) {
        this.restTemplate = restTemplateBuilder.rootUri(baseUrl)
                .build();
    }

    /**
     * Uploads a file to IPFS.
     *
     * @param inputStream The InputStream of the file to upload.
     * @return the CID of the uploaded file.
     */
    @Override
    public String uploadFile(InputStream inputStream) {
        try {
            var body = new LinkedMultiValueMap<>();
            body.set("file", new InputStreamResource(inputStream));

            var headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            var requestEntity = new HttpEntity<>(body, headers);
            var response = restTemplate.postForEntity(UPLOAD_ENDPOINT, requestEntity, IpfsAddView.class);

            return Objects.requireNonNull(response.getBody()).cid();
        } catch (RestClientException | NullPointerException e) {
            logger.error("Error uploading file to IPFS", e);
            throw new RuntimeException("Failed to upload file to IPFS.", e);
        }
    }
}
