package eu.fairkom.faircommons.ipfs_service.models;

import java.util.List;

public record IpfsAddView(
        String name,
        String cid,
        int size,
        List<String> allocations) {
}
