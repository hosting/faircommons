package eu.fairkom.faircommons.ipfs_service.service;

import java.io.InputStream;

public interface IpfsService {
    String uploadFile(InputStream inputStream);
}
