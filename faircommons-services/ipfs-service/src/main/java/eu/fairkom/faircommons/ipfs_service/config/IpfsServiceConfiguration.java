package eu.fairkom.faircommons.ipfs_service.config;

import eu.fairkom.faircommons.common.minio.MinioConfiguration;
import eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration;
import eu.fairkom.faircommons.common.security.SecurityWebAutoConfiguration;
import io.ipfs.api.IPFS;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackageClasses = {RabbitMqConfiguration.class, MinioConfiguration.class})
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@Import(value = {SecurityWebAutoConfiguration.class})
public class IpfsServiceConfiguration {
    @Bean
    @ConditionalOnProperty(prefix = "ipfs", name = "service", havingValue = "daemon")
    IPFS ipfsClient(@Value("${ipfs.host}") String host, @Value("${ipfs.port}") int port) {
        return new IPFS(host, port);
    }
}
