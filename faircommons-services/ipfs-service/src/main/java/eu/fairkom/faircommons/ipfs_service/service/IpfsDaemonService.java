package eu.fairkom.faircommons.ipfs_service.service;

import io.ipfs.api.IPFS;
import io.ipfs.api.NamedStreamable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;

@Service
@ConditionalOnProperty(prefix = "ipfs", name = "service", havingValue = "daemon")
public class IpfsDaemonService implements IpfsService {
    private final IPFS ipfsClient;

    public IpfsDaemonService(IPFS ipfsClient) {
        this.ipfsClient = ipfsClient;
    }

    @Override
    public String uploadFile(InputStream inputStream) {
        try {
            var inputStreamWrapper = new NamedStreamable.InputStreamWrapper(inputStream);
            var merkleNode = ipfsClient.add(inputStreamWrapper).get(0);

            return merkleNode.hash.toBase58();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
