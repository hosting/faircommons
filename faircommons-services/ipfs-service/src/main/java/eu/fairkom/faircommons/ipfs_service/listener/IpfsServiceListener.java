package eu.fairkom.faircommons.ipfs_service.listener;

import eu.fairkom.faircommons.common.rabbitmq.RabbitMqListener;
import eu.fairkom.faircommons.common.rabbitmq.messages.Message;
import eu.fairkom.faircommons.common.rabbitmq.messages.MetaDataMessage;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import eu.fairkom.faircommons.ipfs_service.service.IpfsServiceMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.IPFS_QUEUE;

@Component
@RabbitListener(queues = IPFS_QUEUE)
public class IpfsServiceListener implements RabbitMqListener<Message> {
    private static final Logger logger = LoggerFactory.getLogger(IpfsServiceListener.class);
    private final IpfsServiceMessageHandler messageHandler;

    public IpfsServiceListener(IpfsServiceMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    @Override
    public void receive(Message message) {
        logger.info("Received message of type: {} for work ID: {}", message.type(), message.workId());

        switch (message.type()) {
            case FILE -> messageHandler.handleWorkFileMessage((WorkFileMessage) message);
            case META -> messageHandler.handleMetaDataMessage((MetaDataMessage) message);
            default -> logger.error("Unknown message type: {}", message.type());
        }
    }
}
