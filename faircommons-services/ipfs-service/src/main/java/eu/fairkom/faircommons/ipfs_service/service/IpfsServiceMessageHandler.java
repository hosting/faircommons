package eu.fairkom.faircommons.ipfs_service.service;

import eu.fairkom.faircommons.common.minio.MinioService;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.views.IpfsEntryView;
import eu.fairkom.faircommons.common.rabbitmq.MessageHandler;
import eu.fairkom.faircommons.common.rabbitmq.messages.MetaDataMessage;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

@Service
public class IpfsServiceMessageHandler extends MessageHandler {
    private static final Logger logger = LoggerFactory.getLogger(IpfsServiceMessageHandler.class);
    private final MinioService minioService;
    private final IpfsService ipfsService;

    public IpfsServiceMessageHandler(RabbitTemplate rabbitTemplate,
                                     MinioService minioService,
                                     IpfsService ipfsService) {
        super(rabbitTemplate);
        this.minioService = minioService;
        this.ipfsService = ipfsService;
    }

    @Override
    public void handleWorkFileMessage(WorkFileMessage message) {
        var userId = message.userId();
        var filename = message.workFile().filenameWithPrefix();

        try (var workFileStream = minioService.downloadFile(userId, filename)) {
            var cid = ipfsService.uploadFile(workFileStream);
            var ipfsEntry = new IpfsEntryView(cid);

            logger.info("Work File successfully saved to IPFS. Sending success event for work ID {}", message.workId());
            sendStatusEvent(message.workId(), ipfsEntry, WorkStatus.WorkStatusType.FILE_IPFS_DONE);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void handleMetaDataMessage(MetaDataMessage message) {
        try (var metaDataStream = new ByteArrayInputStream(message.metaData().getBytes(StandardCharsets.UTF_8))) {

            var cid = ipfsService.uploadFile(metaDataStream);
            var ipfsEntry = new IpfsEntryView(cid);

            logger.info("Meta Data successfully saved to IPFS. Sending success event for work ID {}", message.workId());
            sendStatusEvent(message.workId(), ipfsEntry, WorkStatus.WorkStatusType.META_IPFS_DONE);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
