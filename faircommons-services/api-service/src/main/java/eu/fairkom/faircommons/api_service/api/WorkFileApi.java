package eu.fairkom.faircommons.api_service.api;

import eu.fairkom.faircommons.api_service.service.WorkFileService;
import eu.fairkom.faircommons.common.minio.MinioException;
import eu.fairkom.faircommons.common.models.DownloadFileResponse;
import eu.fairkom.faircommons.common.models.views.FileUploadView;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/works")
@CrossOrigin(exposedHeaders = "location")
public class WorkFileApi {
    private final WorkFileService workFileService;

    public WorkFileApi(WorkFileService workFileService) {
        this.workFileService = workFileService;
    }

    @GetMapping("/upload/presigned-url")
    public ResponseEntity<FileUploadView> generatePresignedUploadUrl(@RequestParam String userId,
                                                                     @RequestParam String filename) {
        return ResponseEntity.ok(workFileService.generateUploadUrl(userId, filename));
    }

    @GetMapping(value = "/{workId}/certificate", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<InputStreamResource> downloadRegistrationCertificate(@PathVariable UUID workId) {
        return createDownloadResponseEntity(workFileService.downloadRegistrationCertificate(workId));
    }

    @GetMapping(value = "/{workId}/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<InputStreamResource> downloadWorkById(@PathVariable UUID workId) {
        return createDownloadResponseEntity(workFileService.downloadWorkById(workId));
    }

    @GetMapping("/public/{grid}/download")
    public ResponseEntity<InputStreamResource> downloadWorkByGrid(@PathVariable String grid) {
        return createDownloadResponseEntity(workFileService.downloadWorkByGrid(grid));
    }

    private ResponseEntity<InputStreamResource> createDownloadResponseEntity(DownloadFileResponse fileResponse) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileResponse.filename())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(fileResponse.inputStreamResource());
    }
}
