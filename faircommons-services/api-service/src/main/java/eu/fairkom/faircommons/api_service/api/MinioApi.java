package eu.fairkom.faircommons.api_service.api;

import eu.fairkom.faircommons.common.minio.MinioService;
import eu.fairkom.faircommons.common.minio.QuotaView;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/minio/")
public class MinioApi {
    private final MinioService minioService;

    public MinioApi(MinioService minioService) {
        this.minioService = minioService;
    }

    @PutMapping("/{bucketName}/quota")
    public ResponseEntity<Void> setQuota(
            @PathVariable String bucketName,
            @RequestBody QuotaView quota) {

        minioService.setQuota(bucketName, quota.size(), quota.unit());
        return ResponseEntity.noContent().build();
    }
}
