package eu.fairkom.faircommons.api_service.service;

import eu.fairkom.faircommons.api_service.repository.TagRepository;
import eu.fairkom.faircommons.api_service.repository.WorkRepository;
import eu.fairkom.faircommons.common.IdentifierGenerator;
import eu.fairkom.faircommons.common.mappers.TagMapper;
import eu.fairkom.faircommons.common.mappers.WorkMapper;
import eu.fairkom.faircommons.common.models.WorkFilter;
import eu.fairkom.faircommons.common.models.WorkVisibility;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.views.PublicWorkView;
import eu.fairkom.faircommons.common.models.views.WorkCreateView;
import eu.fairkom.faircommons.common.models.views.WorkView;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class WorkService {
    private static final Logger logger = LoggerFactory.getLogger(WorkService.class);
    private final WorkRepository workRepository;
    private final WorkMapper workMapper;
    private final TagMapper tagMapper;
    private final TagRepository tagRepository;
    private final ApplicationEventPublisher eventPublisher;

    public WorkService(WorkRepository workRepository,
                       WorkMapper workMapper,
                       TagMapper tagMapper,
                       TagRepository tagRepository, ApplicationEventPublisher eventPublisher) {
        this.workRepository = workRepository;
        this.workMapper = workMapper;
        this.tagMapper = tagMapper;
        this.tagRepository = tagRepository;
        this.eventPublisher = eventPublisher;
    }

    @Transactional
    public UUID registerWork(WorkCreateView workCreateView) {
        logger.info("Registering work for user: {}", workCreateView.registrar());
        var work = workRepository.save(createWorkEntity(workCreateView));
        logger.debug("Work saved with ID: {}", work.getId());

        var message = new WorkFileMessage(
                work.getId(),
                workCreateView.workFile(),
                workCreateView.registrar()
        );

        eventPublisher.publishEvent(message);
        return work.getId();
    }

    @Transactional(readOnly = true)
    public Page<PublicWorkView> getWorksByFilter(WorkFilter filter, Pageable pageable) {
        return workRepository.findWorksByFilter(filter, pageable)
                .map(workMapper::toPublicView);
    }

    @Transactional(readOnly = true)
    public Optional<WorkView> getWorkById(UUID workId) {
        return workRepository.findById(workId)
                .map(workMapper::toView);
    }

    public Optional<PublicWorkView> getPublicWorkByGrid(String grid) {
        return workRepository.findWorkByGridAndVisibility(grid, WorkVisibility.PUBLIC)
                .map(workMapper::toPublicView);
    }

    public Page<WorkView> getWorksByUserId(String userId, Pageable pageable) {
        return workRepository.findWorksByRegistrar(userId, pageable)
                .map(workMapper::toView);
    }

    @Transactional
    public Work updateWork(WorkStatus workStatus, WorkStatus.WorkStatusType type) {
        logger.info("Updating work {} with status type: {}", workStatus.getWork().getId(), type);
        var work = workRepository.findById(workStatus.getWork().getId())
                .orElseThrow(EntityNotFoundException::new);

        work.setStatus(workStatus);
        switch (type) {
            case FILE_HASHING_DONE -> work.setHashFile(workStatus.getHashCode());
            case FILE_IPFS_DONE -> work.setIpfsFile(workStatus.getIpfsEntry());
            case META_HASHING_DONE -> work.setHashMeta(workStatus.getHashCode());
            case META_IPFS_DONE -> work.setIpfsMeta(workStatus.getIpfsEntry());
        }

        return workRepository.save(work);
    }

    private Work createWorkEntity(WorkCreateView view) {
        var tags = view.tags()
                .stream()
                .distinct()
                .map(name -> tagRepository.findTagByName(name)
                        .orElseGet(() -> tagMapper.toEntity(name)))
                .toList();

        var workEntity = workMapper.toEntity(workMapper.toWorkView(view));
        workEntity.setTags(tags);
        workEntity.setGrid(IdentifierGenerator.generateGlobalReleaseIdentifier());

        return workEntity;
    }
}
