package eu.fairkom.faircommons.api_service.api;

import eu.fairkom.faircommons.api_service.service.WorkService;
import eu.fairkom.faircommons.common.models.views.WorkCreateView;
import eu.fairkom.faircommons.common.models.views.WorkView;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.UUID;

@RestController
@RequestMapping("/works")
@CrossOrigin(exposedHeaders = "location")
public class WorkApi {

    private final WorkService workService;

    public WorkApi(WorkService workService) {
        this.workService = workService;
    }

    @GetMapping("/{workId}")
    public ResponseEntity<WorkView> getWorkById(@PathVariable UUID workId) {
        return ResponseEntity.of(workService.getWorkById(workId));
    }

    @PutMapping("/{workId}")
    public ResponseEntity<Void> updateWork(@PathVariable UUID workId) {
        return null;
    }

    @PostMapping("/register")
    public ResponseEntity<Void> registerWork(@Valid @RequestBody WorkCreateView workCreateView) {
        var workId = workService.registerWork(workCreateView);
        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentServletMapping()
                        .path("/works/{workId}")
                        .query(null)
                        .build(workId))
                .build();
    }
}
