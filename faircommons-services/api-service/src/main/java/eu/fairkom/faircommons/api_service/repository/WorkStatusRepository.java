package eu.fairkom.faircommons.api_service.repository;

import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface WorkStatusRepository extends JpaRepository<WorkStatus, UUID> {
}
