package eu.fairkom.faircommons.api_service.strategy;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.fairkom.faircommons.common.mappers.WorkMapper;
import eu.fairkom.faircommons.common.models.StorageOption;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.HASHING_QUEUE;
import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.IPFS_QUEUE;

@Service
public class FileHashingDoneDispatcher extends AbstractMessageDispatcher {

    public FileHashingDoneDispatcher(RabbitTemplate rabbitTemplate,
                                     WorkMapper workMapper,
                                     ObjectMapper objectMapper) {
        super(workMapper, objectMapper, rabbitTemplate);
    }

    @Override
    public void dispatch(Work work) {
        var ipfsOption = work.getStorageOptions().stream()
                .filter(option -> option.equals(StorageOption.IPFS))
                .findFirst();

        if (ipfsOption.isPresent()) {
            rabbitTemplate.convertAndSend(IPFS_QUEUE, buildWorkFileMessage(work));
        } else {
            rabbitTemplate.convertAndSend(HASHING_QUEUE, buildMetaDataMessage(work));
        }
    }

    @Override
    public WorkStatus.WorkStatusType getType() {
        return WorkStatus.WorkStatusType.FILE_HASHING_DONE;
    }
}
