package eu.fairkom.faircommons.api_service.service;

import eu.fairkom.faircommons.api_service.strategy.MessageDispatchStrategy;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.rabbitmq.messages.StatusEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
public class WorkMessageDispatcher {
    private final static Logger logger = LoggerFactory.getLogger(WorkMessageDispatcher.class);

    private final Map<WorkStatus.WorkStatusType, MessageDispatchStrategy> dispatchStrategies;
    private final WorkStatusService workStatusService;
    private final WorkService workService;

    public WorkMessageDispatcher(Map<WorkStatus.WorkStatusType, MessageDispatchStrategy> dispatchStrategies,
                                 WorkStatusService workStatusService,
                                 WorkService workService) {
        this.dispatchStrategies = dispatchStrategies;
        this.workStatusService = workStatusService;
        this.workService = workService;
    }

    @Transactional
    public void handleStatusEvent(StatusEvent event) {
        logger.info("Handling status event for work ID: {}", event.workId());
        var workStatus = workStatusService.saveStatus(event.status());
        var work = workService.updateWork(workStatus, workStatus.getType());

        var strategy = dispatchStrategies.get(workStatus.getType());
        if (strategy != null) {
            logger.debug("Dispatching work ID: {} using strategy for type: {}", work.getId(), workStatus.getType());
            strategy.dispatch(work);
        } else {
            logger.error("No dispatching strategy found for type: " + workStatus.getType());
        }
    }
}