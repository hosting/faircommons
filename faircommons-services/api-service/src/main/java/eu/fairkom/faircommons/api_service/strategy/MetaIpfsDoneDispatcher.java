package eu.fairkom.faircommons.api_service.strategy;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.fairkom.faircommons.common.mappers.WorkMapper;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.views.WorkStatusView;
import eu.fairkom.faircommons.common.rabbitmq.messages.StatusEvent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import static eu.fairkom.faircommons.common.models.entities.WorkStatus.WorkStatusType.REGISTRATION_DONE;
import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.STATUS_QUEUE;

@Service
public class MetaIpfsDoneDispatcher extends AbstractMessageDispatcher {
    public MetaIpfsDoneDispatcher(WorkMapper workMapper,
                                  ObjectMapper objectMapper,
                                  RabbitTemplate rabbitTemplate) {
        super(workMapper, objectMapper, rabbitTemplate);
    }

    @Override
    public void dispatch(Work work) {
        var event = new StatusEvent(
                work.getId(),
                new WorkStatusView(
                        work.getId(),
                        REGISTRATION_DONE
                ));

        rabbitTemplate.convertAndSend(STATUS_QUEUE, event);
    }

    @Override
    public WorkStatus.WorkStatusType getType() {
        return WorkStatus.WorkStatusType.META_IPFS_DONE;
    }
}
