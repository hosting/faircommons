package eu.fairkom.faircommons.api_service.api;

import eu.fairkom.faircommons.common.minio.MinioException;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFound() {
        return ResponseEntity.notFound().build();
    }


    @ExceptionHandler(MinioException.class)
    public ResponseEntity<String> handleMinioException() {
        return ResponseEntity.status(503)
                .body("Minio Service temporarily unavailable.");
    }
}
