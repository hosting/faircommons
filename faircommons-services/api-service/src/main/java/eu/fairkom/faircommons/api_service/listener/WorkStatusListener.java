package eu.fairkom.faircommons.api_service.listener;

import eu.fairkom.faircommons.api_service.service.WorkMessageDispatcher;
import eu.fairkom.faircommons.common.rabbitmq.RabbitMqListener;
import eu.fairkom.faircommons.common.rabbitmq.messages.StatusEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.STATUS_QUEUE;

@Component
@RabbitListener(queues = STATUS_QUEUE)
public class WorkStatusListener implements RabbitMqListener<StatusEvent> {
    private static final Logger logger = LoggerFactory.getLogger(WorkStatusListener.class);
    private final WorkMessageDispatcher workMessageDispatcher;

    public WorkStatusListener(WorkMessageDispatcher workMessageDispatcher) {
        this.workMessageDispatcher = workMessageDispatcher;
    }

    @Override
    public void receive(StatusEvent event) {
        try {
            logger.info("Received status message with type: {} and work ID: {}", event.status().type(), event.workId());
            workMessageDispatcher.handleStatusEvent(event);
        } catch (Throwable e) {
            logger.error("Error processing status event with work ID: {}. Message: {}", event.workId(), e.getMessage(), e);
            throw e;
        }
    }
}
