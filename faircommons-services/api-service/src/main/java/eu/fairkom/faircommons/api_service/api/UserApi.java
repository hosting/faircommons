package eu.fairkom.faircommons.api_service.api;

import eu.fairkom.faircommons.api_service.service.WorkService;
import eu.fairkom.faircommons.common.models.views.WorkView;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin(exposedHeaders = "location")
public class UserApi {
    private final WorkService workService;

    public UserApi(WorkService workService) {
        this.workService = workService;
    }

    @GetMapping("/{userId}/works")
    public ResponseEntity<Page<WorkView>> getWorksByUserId(@PathVariable String userId,
                                                           @ParameterObject Pageable pageable) {
        return ResponseEntity.ok(workService.getWorksByUserId(userId, pageable));
    }
}
