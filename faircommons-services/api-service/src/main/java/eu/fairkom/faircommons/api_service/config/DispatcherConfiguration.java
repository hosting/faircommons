package eu.fairkom.faircommons.api_service.config;

import eu.fairkom.faircommons.api_service.strategy.MessageDispatchStrategy;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
public class DispatcherConfiguration {
    @Bean
    public Map<WorkStatus.WorkStatusType, MessageDispatchStrategy> dispatchStrategies(
            List<MessageDispatchStrategy> strategies) {

        return strategies.stream()
                .collect(Collectors.toMap(MessageDispatchStrategy::getType, Function.identity()));
    }
}

