package eu.fairkom.faircommons.api_service.config;

import eu.fairkom.faircommons.common.security.SecurityWebAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = "eu.fairkom.faircommons.common")
@EnableJpaRepositories("eu.fairkom.faircommons.api_service.repository")
@EntityScan("eu.fairkom.faircommons.common.models.entities")
@Import(value = {SecurityWebAutoConfiguration.class})
public class ApiServiceConfiguration {
}
