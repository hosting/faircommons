package eu.fairkom.faircommons.api_service.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.fairkom.faircommons.common.mappers.WorkMapper;
import eu.fairkom.faircommons.common.models.MessageBuildingException;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.rabbitmq.messages.MetaDataMessage;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public abstract class AbstractMessageDispatcher implements MessageDispatchStrategy {
    protected final WorkMapper workMapper;
    protected final ObjectMapper objectMapper;
    protected final RabbitTemplate rabbitTemplate;

    public AbstractMessageDispatcher(WorkMapper workMapper,
                                     ObjectMapper objectMapper,
                                     RabbitTemplate rabbitTemplate) {
        this.workMapper = workMapper;
        this.objectMapper = objectMapper;
        this.rabbitTemplate = rabbitTemplate;
    }

    protected WorkFileMessage buildWorkFileMessage(Work work) {
        var workFileView = workMapper.toWorkFileView(work.getWorkFile());

        return new WorkFileMessage(
                work.getId(),
                workFileView,
                work.getRegistrar()
        );
    }

    protected MetaDataMessage buildMetaDataMessage(Work work) throws MessageBuildingException {
        try {
            var metaData = workMapper.toMetaDataView(work);

            return new MetaDataMessage(
                    work.getId(),
                    objectMapper.writeValueAsString(metaData)
            );
        } catch (JsonProcessingException e) {
            throw new MessageBuildingException("Error while building MetaDataMessage for Work with ID: " + work.getId(), e);
        }
    }

}

