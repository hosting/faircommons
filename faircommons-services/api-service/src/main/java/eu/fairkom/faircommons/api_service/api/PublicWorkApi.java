package eu.fairkom.faircommons.api_service.api;

import eu.fairkom.faircommons.api_service.service.WorkService;
import eu.fairkom.faircommons.common.models.WorkFilter;
import eu.fairkom.faircommons.common.models.views.PublicWorkView;
import jakarta.persistence.EntityNotFoundException;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/works/public")
@CrossOrigin(exposedHeaders = "location")
public class PublicWorkApi {
    private final WorkService workService;

    public PublicWorkApi(WorkService workService) {
        this.workService = workService;
    }

    @GetMapping("/filter")
    public ResponseEntity<Page<PublicWorkView>> getWorksByFilter(@ParameterObject WorkFilter filter,
                                                                 @ParameterObject Pageable pageable) {
        return ResponseEntity.ok(workService.getWorksByFilter(filter, pageable));
    }

    @GetMapping("/{grid}")
    public ResponseEntity<PublicWorkView> getWorkByGrid(@PathVariable String grid) {
        return ResponseEntity.of(workService.getPublicWorkByGrid(grid));
    }
}
