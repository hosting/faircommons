package eu.fairkom.faircommons.api_service.listener;

import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.HASHING_QUEUE;

@Component
public class WorkRegisteredEventListener {
    private static final Logger logger = LoggerFactory.getLogger(WorkRegisteredEventListener.class);

    private final RabbitTemplate rabbitTemplate;

    public WorkRegisteredEventListener(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void handleWorkRegisteredEvent(WorkFileMessage message) {
        rabbitTemplate.convertAndSend(HASHING_QUEUE, message);
        logger.info("Message sent to hashing queue for work ID: {}", message.workId());
    }
}
