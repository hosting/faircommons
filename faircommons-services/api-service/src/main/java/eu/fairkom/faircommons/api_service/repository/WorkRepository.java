package eu.fairkom.faircommons.api_service.repository;

import eu.fairkom.faircommons.common.models.WorkFilter;
import eu.fairkom.faircommons.common.models.WorkVisibility;
import eu.fairkom.faircommons.common.models.entities.Tag_;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.Work_;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface WorkRepository extends JpaRepository<Work, UUID>, JpaSpecificationExecutor<Work> {
    @EntityGraph(value = "work.complete", type = EntityGraph.EntityGraphType.LOAD)
    Page<Work> findWorksByRegistrar(String userId, Pageable pageable);

    Optional<Work> findWorkByGrid(String grid);

    @EntityGraph(value = "work.public", type = EntityGraph.EntityGraphType.LOAD)
    Optional<Work> findWorkByGridAndVisibility(String grid, WorkVisibility workVisibility);

    default Page<Work> findWorksByFilter(WorkFilter filter, Pageable pageable) {
        return findAll(createSpecification(filter), pageable);
    }

    private Specification<Work> createSpecification(WorkFilter workFilter) {
        return (root, query, criteriaBuilder) -> {
            var predicates = new ArrayList<Predicate>();

            //default, only public works are allowed
            predicates.add(criteriaBuilder.equal(root.get(Work_.visibility), WorkVisibility.PUBLIC));

            // Add predicate for type attribute
            if (workFilter.type() != null) {
                predicates.add(criteriaBuilder.equal(root.get(Work_.TYPE), workFilter.type()));
            }

            // Add predicate for title attribute
            if (workFilter.title() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(Work_.TITLE)), "%" + workFilter.title().toLowerCase() + "%"));
            }

            // Add predicate for license identifier attribute
            if (workFilter.licenseIdentifier() != null) {
                predicates.add(criteriaBuilder.equal(root.get(Work_.LICENSE).get("identifier"), workFilter.licenseIdentifier()));
            }
            // Add predicate for tags
            if (workFilter.tags() != null && !workFilter.tags().isEmpty()) {
                var tagsJoin = root.join(Work_.TAGS, JoinType.INNER);
                predicates.add(tagsJoin.get(Tag_.NAME).in(workFilter.tags()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
