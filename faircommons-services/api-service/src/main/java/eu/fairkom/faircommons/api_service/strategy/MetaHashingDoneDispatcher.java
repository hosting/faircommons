package eu.fairkom.faircommons.api_service.strategy;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.fairkom.faircommons.common.mappers.WorkMapper;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.IPFS_QUEUE;

@Service
public class MetaHashingDoneDispatcher extends AbstractMessageDispatcher {
    public MetaHashingDoneDispatcher(WorkMapper workMapper,
                                     ObjectMapper objectMapper,
                                     RabbitTemplate rabbitTemplate) {
        super(workMapper, objectMapper, rabbitTemplate);
    }

    @Override
    public void dispatch(Work work) {
        rabbitTemplate.convertAndSend(IPFS_QUEUE, buildMetaDataMessage(work));
    }

    @Override
    public WorkStatus.WorkStatusType getType() {
        return WorkStatus.WorkStatusType.META_HASHING_DONE;
    }
}
