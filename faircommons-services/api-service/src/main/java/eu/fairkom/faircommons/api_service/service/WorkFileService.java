package eu.fairkom.faircommons.api_service.service;

import eu.fairkom.faircommons.api_service.repository.WorkRepository;
import eu.fairkom.faircommons.common.minio.MinioException;
import eu.fairkom.faircommons.common.minio.MinioService;
import eu.fairkom.faircommons.common.models.DownloadFileResponse;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.views.FileUploadView;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.UUID;

import static eu.fairkom.faircommons.common.Constants.REGISTRATION_CERTIFICATE_NAME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

@Service
public class WorkFileService {
    private static final Logger logger = LoggerFactory.getLogger(WorkFileService.class);
    private final MinioService minioService;
    private final WorkRepository workRepository;

    public WorkFileService(MinioService minioService,
                           WorkRepository workRepository) {
        this.minioService = minioService;
        this.workRepository = workRepository;
    }

    public FileUploadView generateUploadUrl(String userId, String filename) throws MinioException {
        logger.info("Generated upload URL for user: {} and filename: {}", userId, filename);

        var prefix = ZonedDateTime.now().format(ISO_OFFSET_DATE_TIME);
        var uploadUrl = minioService.generatePresignedUploadUrl(userId, prefix + "/" + filename);

        return new FileUploadView(prefix, filename, uploadUrl);
    }

    public DownloadFileResponse downloadRegistrationCertificate(UUID workId) {
        logger.info("Downloading registration certificate for work ID: {}", workId);
        var work = workRepository.findById(workId).orElseThrow(() -> workNotFoundException(workId));
        var inputStream = minioService.downloadRegistrationCertificate(work.getRegistrar(),
                work.getWorkFile().getPrefix());

        return new DownloadFileResponse(new InputStreamResource(inputStream), REGISTRATION_CERTIFICATE_NAME);
    }

    public DownloadFileResponse downloadWorkById(UUID workId) {
        logger.info("Downloading work with ID: {}", workId);
        var work = workRepository.findById(workId).orElseThrow(() -> workNotFoundException(workId));

        return downLoadFile(work);
    }

    public DownloadFileResponse downloadWorkByGrid(String grid) {
        logger.info("Downloading work with grid: {}", grid);
        var work = workRepository.findWorkByGrid(grid).orElseThrow(() -> workNotFoundException(grid));

        return downLoadFile(work);
    }

    private DownloadFileResponse downLoadFile(Work work) {
        var inputStream = minioService.downloadFile(work.getRegistrar(), work.getWorkFile().getFilenameWithPrefix());

        return new DownloadFileResponse(new InputStreamResource(inputStream), work.getWorkFile().getFilename());
    }

    private EntityNotFoundException workNotFoundException(UUID workId) {
        logger.warn("Work with ID {} not found", workId);
        return new EntityNotFoundException("Work with ID " + workId + " not found");
    }

    private EntityNotFoundException workNotFoundException(String grid) {
        logger.warn("Work with grid {} not found", grid);
        return new EntityNotFoundException("Work with grid " + grid + " not found");
    }
}
