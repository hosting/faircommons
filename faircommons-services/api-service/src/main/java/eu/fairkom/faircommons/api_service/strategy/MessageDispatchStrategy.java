package eu.fairkom.faircommons.api_service.strategy;

import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;

public interface MessageDispatchStrategy {
    void dispatch(Work work);

    WorkStatus.WorkStatusType getType();
}
