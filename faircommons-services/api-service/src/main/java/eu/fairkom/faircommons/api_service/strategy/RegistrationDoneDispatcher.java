package eu.fairkom.faircommons.api_service.strategy;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.fairkom.faircommons.common.mappers.WorkMapper;
import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.rabbitmq.messages.PostRegistrationMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.POST_REGISTRATION_EXCHANGE;
import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.POST_REGISTRATION_QUEUE;

@Service
public class RegistrationDoneDispatcher extends AbstractMessageDispatcher {
    public RegistrationDoneDispatcher(WorkMapper workMapper,
                                      ObjectMapper objectMapper,
                                      RabbitTemplate rabbitTemplate) {
        super(workMapper, objectMapper, rabbitTemplate);
    }

    @Override
    public void dispatch(Work work) {
        var message = new PostRegistrationMessage(workMapper.toView(work));

        rabbitTemplate.convertAndSend(POST_REGISTRATION_EXCHANGE, POST_REGISTRATION_QUEUE, message);
    }

    @Override
    public WorkStatus.WorkStatusType getType() {
        return WorkStatus.WorkStatusType.REGISTRATION_DONE;
    }
}
