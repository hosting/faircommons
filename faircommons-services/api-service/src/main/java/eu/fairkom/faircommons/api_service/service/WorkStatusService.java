package eu.fairkom.faircommons.api_service.service;

import eu.fairkom.faircommons.api_service.repository.WorkStatusRepository;
import eu.fairkom.faircommons.common.mappers.WorkStatusMapper;
import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.views.WorkStatusView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WorkStatusService {
    private static final Logger logger = LoggerFactory.getLogger(WorkStatusService.class);
    private final WorkStatusRepository workStatusRepository;
    private final WorkStatusMapper workStatusMapper;

    public WorkStatusService(WorkStatusRepository workStatusRepository, WorkStatusMapper workStatusMapper) {
        this.workStatusRepository = workStatusRepository;
        this.workStatusMapper = workStatusMapper;
    }

    @Transactional
    public WorkStatus saveStatus(WorkStatusView workStatusView) {
        logger.info("Saving work status for work ID: {}", workStatusView.workId());
        var workStatus = workStatusMapper.toEntity(workStatusView);
        return workStatusRepository.save(workStatus);
    }
}
