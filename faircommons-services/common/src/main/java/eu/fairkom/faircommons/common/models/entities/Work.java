package eu.fairkom.faircommons.common.models.entities;

import eu.fairkom.faircommons.common.models.StorageOption;
import eu.fairkom.faircommons.common.models.WorkType;
import eu.fairkom.faircommons.common.models.WorkVisibility;
import eu.fairkom.faircommons.common.models.entities.embedded.*;
import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.time.ZonedDateTime;
import java.util.*;

@Entity
@NamedEntityGraph(
        name = "work.public",
        attributeNodes = {
                @NamedAttributeNode("tags"),
                @NamedAttributeNode("creator")
        }
)
@NamedEntityGraph(
        name = "work.complete",
        attributeNodes = {
                @NamedAttributeNode("tags"),
                @NamedAttributeNode("creator"),
                @NamedAttributeNode("status"),
                @NamedAttributeNode("contributors")
        }
)
public class Work extends Identifiable<UUID> {
    @Id
    @GeneratedValue
    @UuidGenerator
    private UUID id;

    private WorkType type;

    @Column(length = 100, updatable = false)
    private String title;

    @Column(length = 2000, updatable = false)
    private String description;

    @Column(length = 36, updatable = false)
    private String registrar;

    @Column(updatable = false)
    private ZonedDateTime registrationDate = ZonedDateTime.now();

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "prefix", column = @Column(name = "work_file_prefix")),
            @AttributeOverride(name = "filename", column = @Column(name = "work_file_filename"))
    })
    private WorkFile workFile;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "work_tag",
            joinColumns = @JoinColumn(name = "work_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags = new ArrayList<>();

    @Column(length = 2)
    private String language;

    private ZonedDateTime creationDate;

    private ZonedDateTime publishingDate; //changing visibility from private to public

    private String publisher;

    private String identifier;

    private WorkVisibility visibility;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "creator_id", nullable = false)
    private Contributor creator; //immutable, main owner

    @OneToMany(mappedBy = "workAsContributor", cascade = CascadeType.ALL)
    private Set<Contributor> contributors = new HashSet<>();

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "identifier", column = @Column(name = "license_identifier")),
            @AttributeOverride(name = "addons", column = @Column(name = "license_addons")),
            @AttributeOverride(name = "dual", column = @Column(name = "license_dual")),
            @AttributeOverride(name = "dalicc", column = @Column(name = "license_dalicc"))
    })
    private License license;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "date", column = @Column(name = "revocation_date")),
            @AttributeOverride(name = "note", column = @Column(name = "revocation_note"))
    })
    private Revocation revocation;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "algorithm", column = @Column(name = "hash_file_algorithm")),
            @AttributeOverride(name = "hexValue", column = @Column(name = "hash_file_hex_value"))
    })
    private HashCode hashFile;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "algorithm", column = @Column(name = "hash_meta_algorithm")),
            @AttributeOverride(name = "hexValue", column = @Column(name = "hash_meta_hex_value"))
    })
    private HashCode hashMeta;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "cid", column = @Column(name = "ipfs_file_cid")),
            @AttributeOverride(name = "path", column = @Column(name = "ipfs_file_path"))
    })
    private IpfsEntry ipfsFile;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "cid", column = @Column(name = "ipfs_meta_cid")),
            @AttributeOverride(name = "path", column = @Column(name = "ipfs_meta_path"))
    })
    private IpfsEntry ipfsMeta;

    @ManyToOne
    private Collection collection;

    @OneToOne
    private WorkStatus status;

    @OrderBy("created")
    @OneToMany(mappedBy = "work", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<WorkStatus> statuses = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<StorageOption> storageOptions = new HashSet<>();

    @Column(unique = true)
    private String grid;

    public void addContributor(Contributor contributor) {
        addBidirectional(this, contributor, Work::getContributors, Contributor::setWorkAsContributor);
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public WorkType getType() {
        return type;
    }

    public void setType(WorkType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public ZonedDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(ZonedDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public WorkFile getWorkFile() {
        return workFile;
    }

    public void setWorkFile(WorkFile workFile) {
        this.workFile = workFile;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(ZonedDateTime publishingDate) {
        this.publishingDate = publishingDate;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public WorkVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(WorkVisibility visibility) {
        this.visibility = visibility;
    }

    public Contributor getCreator() {
        return creator;
    }

    public void setCreator(Contributor creator) {
        this.creator = creator;
    }

    public Set<Contributor> getContributors() {
        return contributors;
    }

    public void setContributors(Set<Contributor> contributors) {
        this.contributors = contributors;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public Revocation getRevocation() {
        return revocation;
    }

    public void setRevocation(Revocation revocation) {
        this.revocation = revocation;
    }

    public HashCode getHashFile() {
        return hashFile;
    }

    public void setHashFile(HashCode hashFile) {
        this.hashFile = hashFile;
    }

    public HashCode getHashMeta() {
        return hashMeta;
    }

    public void setHashMeta(HashCode hashMeta) {
        this.hashMeta = hashMeta;
    }

    public IpfsEntry getIpfsFile() {
        return ipfsFile;
    }

    public void setIpfsFile(IpfsEntry ipfsFile) {
        this.ipfsFile = ipfsFile;
    }

    public IpfsEntry getIpfsMeta() {
        return ipfsMeta;
    }

    public void setIpfsMeta(IpfsEntry ipfsMeta) {
        this.ipfsMeta = ipfsMeta;
    }

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    public WorkStatus getStatus() {
        return status;
    }

    public void setStatus(WorkStatus status) {
        this.status = status;
    }

    public Set<WorkStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<WorkStatus> statuses) {
        this.statuses = statuses;
    }

    public Set<StorageOption> getStorageOptions() {
        return storageOptions;
    }

    public void setStorageOptions(Set<StorageOption> storageOptions) {
        this.storageOptions = storageOptions;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }
}
