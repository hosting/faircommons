package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.Identifiable;
import eu.fairkom.faircommons.common.models.views.IdentifiableView;
import jakarta.persistence.EntityManager;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.UUID;

@Component
final class EntityMapper {
    private final EntityManager entityManager;

    public EntityMapper(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public <ID extends Serializable, T extends Identifiable<ID>> T mapReference(@TargetType Class<T> entityClass, UUID id) {
        return id != null
                ? entityManager.getReference(entityClass, id)
                : null;
    }


    @ObjectFactory
    public <ID extends Serializable, T> T getOrCreate(@TargetType Class<T> entityClass, IdentifiableView<ID> view) {
        return view.id() != null
                ? entityManager.getReference(entityClass, view.id())
                : BeanUtils.instantiateClass(entityClass);
    }
}
