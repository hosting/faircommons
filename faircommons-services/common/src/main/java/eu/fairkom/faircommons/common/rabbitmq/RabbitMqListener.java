package eu.fairkom.faircommons.common.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;

public interface RabbitMqListener<T> {
    @RabbitHandler
    void receive(T message);
}
