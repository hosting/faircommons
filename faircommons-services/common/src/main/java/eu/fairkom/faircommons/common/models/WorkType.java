package eu.fairkom.faircommons.common.models;

public enum WorkType {
    HARDWARE,
    DATA,
    INTERACTIVE,
    SOFTWARE,
    VIDEO,
    PICTURE,
    DOCUMENT,
    AUDIO
}
