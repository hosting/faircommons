package eu.fairkom.faircommons.common.models.views;

public record PublicAuthorView(
        String pseudonym,
        String url) {
}