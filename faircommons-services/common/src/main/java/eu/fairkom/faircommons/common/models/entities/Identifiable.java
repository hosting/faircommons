package eu.fairkom.faircommons.common.models.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.Function;

public abstract class Identifiable<ID extends Serializable> implements Serializable {
    /**
     * Helper method to add a new element to a bidirectional OneToMany relation. It adds the new element to the
     * collection and also sets the back-pointing reference on the element (i.e. both sides of the bidirectional
     * relation)
     *
     * @param entity        the entity containing the collection
     * @param newValue      the new element to add to the collection
     * @param getter        the getter on the entity type to retrieve the collection
     * @param reverseSetter the setter on the element type to set the back-pointing reference
     * @param <T>           the type of the entity
     * @param <A>           the type of the elements in the collection
     * @param <C>           the type of the collection
     */
    static <ID extends Serializable, T extends Identifiable<ID>, A extends Identifiable<ID>, C extends Collection<A>> void addBidirectional(T entity, A newValue, Function<T, C> getter, BiConsumer<A, T> reverseSetter) {
        getter.apply(entity).add(newValue);
        reverseSetter.accept(newValue, entity);
    }

    public abstract ID getId();
}
