package eu.fairkom.faircommons.common.rabbitmq.messages;

import eu.fairkom.faircommons.common.models.views.WorkFileView;

import java.util.UUID;


public record WorkFileMessage(
        UUID workId,
        WorkFileView workFile,
        String userId,
        MessageType type) implements Message {

    public WorkFileMessage(UUID workId, WorkFileView workFile, String userId) {
        this(workId, workFile, userId, MessageType.FILE);
    }
}
