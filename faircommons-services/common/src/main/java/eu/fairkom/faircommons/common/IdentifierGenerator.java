package eu.fairkom.faircommons.common;

import java.util.concurrent.ThreadLocalRandom;

public class IdentifierGenerator {
    private static final String IDENTIFIER_SCHEME = "RC";
    private static final String ISSUER_CODE = "FAREG";
    private static final int UNIQUE_NUMBER_LENGTH = 10;

    /**
     * Generates a globally unique release identifier.
     *
     * @return the generated identifier
     */
    public static String generateGlobalReleaseIdentifier() {
        var releaseNumber = generateReleaseNumber();
        var checkCharacter = calculateCheckCharacter(releaseNumber);

        return IDENTIFIER_SCHEME +
                "-" +
                ISSUER_CODE +
                "-" +
                releaseNumber +
                "-" +
                checkCharacter;
    }

    private static String generateReleaseNumber() {
        long releaseNumber = ThreadLocalRandom.current().nextLong((long) Math.pow(10, UNIQUE_NUMBER_LENGTH));
        return String.format("%010d", releaseNumber);
    }

    public static char calculateCheckCharacter(String input) {
        var base = 36;
        var modulus = 37;

        var sum = input.chars()
                .map(c -> Character.digit(c, base))
                .reduce(0, (acc, digit) -> ((acc + digit) % modulus) * base % modulus);

        var quotient = (modulus - sum) % modulus;
        if (quotient == 0) {
            quotient = modulus - 1;
        }

        var remainder = (quotient * (base - 1)) % modulus;
        var checkDigitValue = (modulus - remainder) % modulus;

        // Check if checkDigitValue is alphanumeric
        var checkChar = (checkDigitValue < 10) ? (char) (checkDigitValue + '0') : (char) (checkDigitValue - 10 + 'A');
        if (Character.isLetterOrDigit(checkChar)) {
            return checkChar;
        } else {
            // If checkChar is not alphanumeric, return a default value
            return 'X';
        }
    }
}