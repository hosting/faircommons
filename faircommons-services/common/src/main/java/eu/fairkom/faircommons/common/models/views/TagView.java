package eu.fairkom.faircommons.common.models.views;

public record TagView(
        Long id,
        String name) implements IdentifiableView<Long> {
}
