package eu.fairkom.faircommons.common.models.entities.embedded;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class WorkFile {
    @Column(updatable = false)
    String prefix;
    @Column(updatable = false)
    String filename;

    public String getFilenameWithPrefix() {
        return prefix + "/" + filename;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
