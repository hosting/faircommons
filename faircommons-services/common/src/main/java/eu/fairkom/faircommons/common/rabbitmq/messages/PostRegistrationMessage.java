package eu.fairkom.faircommons.common.rabbitmq.messages;

import eu.fairkom.faircommons.common.models.views.WorkView;

import java.io.Serializable;

public record PostRegistrationMessage(WorkView work) implements Serializable {
}
