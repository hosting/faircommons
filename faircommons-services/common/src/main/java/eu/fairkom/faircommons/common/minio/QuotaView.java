package eu.fairkom.faircommons.common.minio;

import io.minio.admin.QuotaUnit;

public record QuotaView(long size, QuotaUnit unit) {}