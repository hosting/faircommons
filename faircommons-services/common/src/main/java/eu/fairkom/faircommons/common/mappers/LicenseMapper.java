package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.embedded.License;
import eu.fairkom.faircommons.common.models.views.LicenseView;
import org.mapstruct.Mapper;

@Mapper(config = MappingConfig.class)
public interface LicenseMapper {
    License toEntity(LicenseView licenseView);

    LicenseView toView(License license);
}
