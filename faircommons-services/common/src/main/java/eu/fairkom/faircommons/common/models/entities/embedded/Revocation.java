package eu.fairkom.faircommons.common.models.entities.embedded;

import jakarta.persistence.Embeddable;

import java.time.ZonedDateTime;

@Embeddable
public class Revocation {
    private ZonedDateTime date;
    private String note;

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
