package eu.fairkom.faircommons.common.security;

import eu.fairkom.faircommons.common.security.models.ConfigurerOrders;
import eu.fairkom.faircommons.common.security.models.UnsecuredProperties;
import eu.fairkom.faircommons.common.security.models.WebSecurityProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static org.springframework.security.config.Customizer.withDefaults;

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(prefix = WebSecurityProperties.PREFIX, name = "enabled", matchIfMissing = true)
@ConditionalOnWebApplication
@EnableConfigurationProperties(value = {UnsecuredProperties.class})
@EnableWebSecurity
public class UnsecuredAutoConfigurer {
    private final UnsecuredProperties unsecuredProperties;

    public UnsecuredAutoConfigurer(UnsecuredProperties unsecuredProperties) {
        this.unsecuredProperties = unsecuredProperties;
    }

    @Bean
    @Order(ConfigurerOrders.UNSECURED)
    public SecurityFilterChain unsecuredFilterChain(HttpSecurity http) throws Exception {
        http.securityMatcher(buildUnsecuredRequestMatcher())
                .cors(withDefaults())
                .csrf(CsrfConfigurer::disable)
                .authorizeHttpRequests(authorize ->
                        authorize.anyRequest().permitAll());


        return http.build();
    }

    private RequestMatcher buildUnsecuredRequestMatcher() {
        return unsecuredProperties.paths().stream()
                .map(AntPathRequestMatcher::new)
                .collect(collectingAndThen(Collectors.<RequestMatcher>toList(), OrRequestMatcher::new));
    }
}
