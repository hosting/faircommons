package eu.fairkom.faircommons.common.security.models;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.DefaultValue;

@ConfigurationProperties(WebSecurityProperties.PREFIX)
public record WebSecurityProperties(@DefaultValue("true") boolean enabled) {
    public static final String PREFIX = "security.web";
}
