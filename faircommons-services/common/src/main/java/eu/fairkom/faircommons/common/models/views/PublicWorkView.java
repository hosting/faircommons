package eu.fairkom.faircommons.common.models.views;

import eu.fairkom.faircommons.common.models.WorkType;

import java.time.ZonedDateTime;
import java.util.List;

public record PublicWorkView(
        WorkType type,
        String title,
        String description,
        List<String> tags,
        String registrar,
        ZonedDateTime registrationDate,
        ZonedDateTime creationDate,
        String publisher,
        String identifier,
        String language,
        PublicContributorView creator,
        LicenseView license,
        String grid,
        String filename) {
}
