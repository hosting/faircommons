package eu.fairkom.faircommons.common;

public final class Constants {
    public static final String REGISTRATION_CERTIFICATE_NAME = "fairregister-certificate.pdf";
    public static final String REGISTRATION_CERTIFICATE_FOLDER = "certificate";
    private Constants() {
    }
}
