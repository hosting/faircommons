package eu.fairkom.faircommons.common.security.models;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.HttpMethod;

import java.util.List;
import java.util.Set;

@ConfigurationProperties(ResourceProperties.PREFIX)
public record ResourceProperties(
        @DefaultValue List<SecuredPath> paths,
        String defaultScope) {
    public static final String PREFIX = WebSecurityProperties.PREFIX + ".resource";

    public record SecuredPath(
            String path,
            List<String> scopes,
            @DefaultValue({
                    "GET",
                    "HEAD",
                    "POST",
                    "PUT",
                    "PATCH",
                    "DELETE",
                    "OPTIONS",
                    "TRACE"}) Set<HttpMethod> methods
    ) {
    }
}
