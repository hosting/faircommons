package eu.fairkom.faircommons.common.models.entities.embedded;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class License {
    @Column(updatable = false)
    private String identifier; //immutable SPDX
    private String addons;
    private String dual;
    private String dalicc;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getAddons() {
        return addons;
    }

    public void setAddons(String addons) {
        this.addons = addons;
    }

    public String getDual() {
        return dual;
    }

    public void setDual(String dual) {
        this.dual = dual;
    }

    public String getDalicc() {
        return dalicc;
    }

    public void setDalicc(String dalicc) {
        this.dalicc = dalicc;
    }
}
