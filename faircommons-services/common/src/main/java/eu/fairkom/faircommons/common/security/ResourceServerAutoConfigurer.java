package eu.fairkom.faircommons.common.security;


import eu.fairkom.faircommons.common.security.models.ConfigurerOrders;
import eu.fairkom.faircommons.common.security.models.ResourceProperties;
import eu.fairkom.faircommons.common.security.models.WebSecurityProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static org.springframework.security.config.Customizer.withDefaults;

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(prefix = WebSecurityProperties.PREFIX, name = "enabled", matchIfMissing = true)
@ConditionalOnWebApplication
@EnableConfigurationProperties(value = {WebSecurityProperties.class, ResourceProperties.class,
        OAuth2ResourceServerProperties.class})
@EnableWebSecurity
public class ResourceServerAutoConfigurer {
    private static final Logger logger = LoggerFactory.getLogger(ResourceServerAutoConfigurer.class);
    private final ResourceProperties resourceProperties;

    public ResourceServerAutoConfigurer(ResourceProperties resourceProperties) {
        this.resourceProperties = resourceProperties;
    }

    @Bean
    @Order(ConfigurerOrders.RESOURCE)
    public SecurityFilterChain resourceFilterChain(HttpSecurity http) throws Exception {

        http.cors(withDefaults())
                .csrf(CsrfConfigurer::disable)
                .authorizeHttpRequests(authorize -> {
                    configureCustomAuth(authorize);
                    configureDefaultAuth(authorize);
                })
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);

        return http.build();
    }

    @Bean
    public JwtDecoder jwtDecoder(OAuth2ResourceServerProperties properties) {
        return JwtDecoders.fromIssuerLocation(properties.getJwt().getIssuerUri());
    }

    private void configureDefaultAuth(AuthorizeHttpRequestsConfigurer<HttpSecurity>.AuthorizationManagerRequestMatcherRegistry authorize) {
        if (resourceProperties.defaultScope() != null) {
            logger.trace("Securing all remaining requests with {}", resourceProperties.defaultScope());
            authorize.anyRequest().hasAuthority("SCOPE_" + resourceProperties.defaultScope());
        } else {
            logger.info("Securing all remaining requests with authenticated user.");
            authorize.anyRequest().authenticated();
        }
    }

    private void configureCustomAuth(AuthorizeHttpRequestsConfigurer<HttpSecurity>.AuthorizationManagerRequestMatcherRegistry authorize) {
        resourceProperties.paths().forEach(securedPath -> {
            logger.info("Securing {} requests to {} with {}",
                    securedPath.methods(), securedPath.path(), securedPath.scopes());
            authorize.requestMatchers(matcher(securedPath))
                    .hasAnyAuthority(asAuthorities(securedPath.scopes()));
        });
    }

    private String[] asAuthorities(List<String> scopes) {
        return scopes.stream()
                .map(scope -> "SCOPE_" + scope)
                .toArray(String[]::new);
    }

    private RequestMatcher matcher(ResourceProperties.SecuredPath securedPath) {
        return securedPath.methods().stream()
                .map(method -> new AntPathRequestMatcher(securedPath.path(), method.name()))
                .collect(collectingAndThen(Collectors.<RequestMatcher>toList(), OrRequestMatcher::new));
    }
}