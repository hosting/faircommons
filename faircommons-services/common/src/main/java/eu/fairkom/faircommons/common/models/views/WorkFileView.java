package eu.fairkom.faircommons.common.models.views;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

public record WorkFileView(
        @Size(max = 255) @NotEmpty String prefix,
        @Size(max = 255) @NotEmpty String filename) implements Serializable {

    public String filenameWithPrefix() {
        return prefix() + "/" + filename();
    }
}
