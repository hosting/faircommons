package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.entities.embedded.HashCode;
import eu.fairkom.faircommons.common.models.entities.embedded.IpfsEntry;
import eu.fairkom.faircommons.common.models.views.HashCodeView;
import eu.fairkom.faircommons.common.models.views.IpfsEntryView;
import eu.fairkom.faircommons.common.models.views.WorkStatusView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MappingConfig.class)
public interface WorkStatusMapper {
    @Mapping(target = "work", source = "workId")
    @Mapping(target = "hashCode", source = "hash")
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "id", ignore = true)
    WorkStatus toEntity(WorkStatusView workStatusView);

    HashCode toHashCodeEntity(HashCodeView hashCodeView);

    IpfsEntry toIpfsEntryEntity(IpfsEntryView ipfsEntryView);
}
