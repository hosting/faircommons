package eu.fairkom.faircommons.common.models.views;

import eu.fairkom.faircommons.common.models.StorageOption;
import eu.fairkom.faircommons.common.models.WorkType;
import eu.fairkom.faircommons.common.models.WorkVisibility;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.time.ZonedDateTime;
import java.util.List;

public record WorkCreateView(
        @NotNull WorkType type,
        @Size(max = 100) @NotBlank String title,
        @Size(max = 2000) @NotBlank String description,
        List<String> tags,
        @Size(max = 2) String language,
        ZonedDateTime creationDate,
        @Size(max = 36) @NotBlank String registrar,
        @Valid ContributorView creator,
        @Valid List<ContributorView> contributors,
        @Size(max = 255) String publisher,
        @Size(max = 255) String identifier,
        @Valid WorkFileView workFile,
        @NotNull WorkVisibility visibility,
        List<StorageOption> storageOptions,
        @Valid LicenseView license,
        @Pattern(regexp = "RC-(01|FAREG)-(LIZ\\d{10}|\\d{10})-([A-Z]|\\d)(?=$)") String grid,
        ZonedDateTime registrationDate
) {

    public WorkCreateView {
        if (storageOptions == null) {
            storageOptions = List.of(StorageOption.FAIRREGISTER_FILE_SYSTEM);
        }
    }
}
