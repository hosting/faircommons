package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.Contributor;
import eu.fairkom.faircommons.common.models.views.ContributorView;
import eu.fairkom.faircommons.common.models.views.PublicContributorView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.UUID;

@Mapper(config = MappingConfig.class, uses = {AuthorMapper.class})
public abstract class ContributorMapper extends BaseMapper<Contributor, ContributorView, UUID> {
    public ContributorMapper() {
        super(Contributor.class);
    }

    @Mapping(target = "workAsCreator", ignore = true)
    @Mapping(target = "workAsContributor", ignore = true)
    public abstract void toEntity(ContributorView contributorView, @MappingTarget Contributor contributor);

    public abstract PublicContributorView toPublicView(Contributor contributor);
}
