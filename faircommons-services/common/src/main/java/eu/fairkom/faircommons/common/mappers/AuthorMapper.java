package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.Author;
import eu.fairkom.faircommons.common.models.views.AuthorView;
import eu.fairkom.faircommons.common.models.views.PublicAuthorView;
import org.mapstruct.Mapper;

import java.util.UUID;

@Mapper(config = MappingConfig.class)
public abstract class AuthorMapper extends BaseMapper<Author, AuthorView, UUID> {
    public AuthorMapper() {
        super(Author.class);
    }

    public abstract PublicAuthorView toPublicView(Author author);
}
