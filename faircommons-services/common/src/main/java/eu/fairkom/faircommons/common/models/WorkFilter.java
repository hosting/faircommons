package eu.fairkom.faircommons.common.models;

import java.util.List;

public record WorkFilter(
        WorkType type,
        String title,
        List<String> tags,
        String licenseIdentifier) {
}
