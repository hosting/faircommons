package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.Identifiable;
import eu.fairkom.faircommons.common.models.views.IdentifiableView;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

public abstract class BaseMapper<E extends Identifiable<ID>, V extends IdentifiableView<ID>, ID extends Serializable> {
    private final Class<E> entityClass;
    protected EntityObjectFactory objectFactory;

    public BaseMapper(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    public abstract V toView(E entity);

    public abstract void toEntity(V view, @MappingTarget E entity);

    public final E toEntity(V view) {
        var service = objectFactory.getOrCreate(entityClass, view);
        toEntity(view, service);

        return service;
    }

    @Autowired
    protected void setObjectFactory(EntityObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
    }
}
