package eu.fairkom.faircommons.common.minio;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.DefaultValue;

@ConfigurationProperties("minio")
public record MinioProperties(
        String url,
        String accessKey,
        String secretKey,
        boolean secure,
        Bucket bucket,
        @DefaultValue("eu-central-1") String region
) {
    record Bucket(
            @DefaultValue("fairregister-") String prefix,
            @DefaultValue("200") Long quota) {
    }
}
