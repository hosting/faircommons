package eu.fairkom.faircommons.common.models.views;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

public record LicenseView(
        @Size(max = 255) @NotEmpty String identifier,
        @Size(max = 255) String addons,
        @Size(max = 255) String dual,
        @Size(max = 255) String dalicc) implements Serializable {
}
