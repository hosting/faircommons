package eu.fairkom.faircommons.common.models.views;

import java.io.Serializable;

public interface IdentifiableView<ID extends Serializable> extends Serializable {
    ID id();
}
