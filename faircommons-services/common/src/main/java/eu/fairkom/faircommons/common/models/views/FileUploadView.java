package eu.fairkom.faircommons.common.models.views;

public record FileUploadView(
        String prefix,
        String filename,
        String url) {
}