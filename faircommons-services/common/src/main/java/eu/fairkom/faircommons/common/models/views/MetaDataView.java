package eu.fairkom.faircommons.common.models.views;

import eu.fairkom.faircommons.common.models.entities.embedded.HashCode;
import eu.fairkom.faircommons.common.models.entities.embedded.IpfsEntry;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

public record MetaDataView(
        UUID workId,
        String title,
        String description,
        ZonedDateTime creationDate,
        ZonedDateTime registrationDate,
        String registrar,
        WorkFileView workFile,
        HashCode hashFile,
        HashCode hashMeta,
        IpfsEntry ipfsFile,
        IpfsEntry ipfsMeta,
        String licenseIdentifier) implements Serializable {
}