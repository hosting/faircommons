package eu.fairkom.faircommons.common.rabbitmq.messages;

import java.io.Serializable;

public enum MessageType implements Serializable {
    FILE, META, SUCCESS, ERROR, POST_REGISTRATION
}
