package eu.fairkom.faircommons.common.models.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

@Entity
public class Contributor extends Identifiable<UUID> {
    @Id
    @GeneratedValue
    @UuidGenerator
    private UUID id;

    @OneToOne(mappedBy = "creator", fetch = FetchType.LAZY)
    private Work workAsCreator;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "work_as_contributor_id")
    private Work workAsContributor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "author_id", nullable = false)
    private Author author;
    private Integer percentage;

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Work getWorkAsCreator() {
        return workAsCreator;
    }

    public void setWorkAsCreator(Work workAsCreator) {
        this.workAsCreator = workAsCreator;
    }

    public Work getWorkAsContributor() {
        return workAsContributor;
    }

    public void setWorkAsContributor(Work workAsContributor) {
        this.workAsContributor = workAsContributor;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
