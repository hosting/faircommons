package eu.fairkom.faircommons.common.rabbitmq.messages;

import java.util.UUID;

public record MetaDataMessage(
        UUID workId,
        String metaData,
        MessageType type) implements Message {

    public MetaDataMessage(UUID workId, String metaData) {
        this(workId, metaData, MessageType.META);
    }
}
