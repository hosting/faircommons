package eu.fairkom.faircommons.common.models;

import org.springframework.core.io.InputStreamResource;

public record DownloadFileResponse(
        InputStreamResource inputStreamResource,
        String filename) {
}
