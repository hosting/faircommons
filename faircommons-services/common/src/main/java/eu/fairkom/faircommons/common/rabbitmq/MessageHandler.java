package eu.fairkom.faircommons.common.rabbitmq;

import eu.fairkom.faircommons.common.models.entities.WorkStatus;
import eu.fairkom.faircommons.common.models.views.HashCodeView;
import eu.fairkom.faircommons.common.models.views.IpfsEntryView;
import eu.fairkom.faircommons.common.models.views.WorkStatusView;
import eu.fairkom.faircommons.common.rabbitmq.messages.MetaDataMessage;
import eu.fairkom.faircommons.common.rabbitmq.messages.StatusEvent;
import eu.fairkom.faircommons.common.rabbitmq.messages.WorkFileMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.UUID;

import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.STATUS_EXCHANGE;
import static eu.fairkom.faircommons.common.rabbitmq.RabbitMqConfiguration.STATUS_QUEUE;

public abstract class MessageHandler {
    private final RabbitTemplate rabbitTemplate;

    protected MessageHandler(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public abstract void handleWorkFileMessage(WorkFileMessage message);

    public abstract void handleMetaDataMessage(MetaDataMessage message);

    public void handleError(UUID workId, Throwable e) {
        var statusEvent = new StatusEvent(
                workId,
                new WorkStatusView(
                        workId,
                        e.getMessage(),
                        WorkStatus.WorkStatusType.ERROR
                )
        );

        rabbitTemplate.convertAndSend(STATUS_EXCHANGE, STATUS_QUEUE, statusEvent);
    }

    protected void sendStatusEvent(UUID workId,
                                   HashCodeView hashCode,
                                   WorkStatus.WorkStatusType statusType) {
        var statusEvent = new StatusEvent(
                workId,
                new WorkStatusView(
                        workId,
                        hashCode,
                        statusType
                )
        );
        rabbitTemplate.convertAndSend(STATUS_EXCHANGE, STATUS_QUEUE, statusEvent);
    }

    protected void sendStatusEvent(UUID workId,
                                   IpfsEntryView ipfsEntry,
                                   WorkStatus.WorkStatusType statusType) {
        var statusEvent = new StatusEvent(
                workId,
                new WorkStatusView(
                        workId,
                        ipfsEntry,
                        statusType
                )
        );
        rabbitTemplate.convertAndSend(STATUS_EXCHANGE, STATUS_QUEUE, statusEvent);
    }
}
