package eu.fairkom.faircommons.common.models;

import java.io.Serializable;

public enum StorageOption implements Serializable {
    FAIRREGISTER_FILE_SYSTEM, IPFS, OWN_STORAGE
}
