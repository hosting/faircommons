package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.Work;
import eu.fairkom.faircommons.common.models.entities.embedded.HashCode;
import eu.fairkom.faircommons.common.models.entities.embedded.IpfsEntry;
import eu.fairkom.faircommons.common.models.entities.embedded.Revocation;
import eu.fairkom.faircommons.common.models.entities.embedded.WorkFile;
import eu.fairkom.faircommons.common.models.views.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.UUID;

@Mapper(config = MappingConfig.class, uses = {ContributorMapper.class, LicenseMapper.class, TagMapper.class})
public abstract class WorkMapper extends BaseMapper<Work, WorkView, UUID> {

    public WorkMapper() {
        super(Work.class);
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "publishingDate", ignore = true)
    @Mapping(target = "revocation", ignore = true)
    @Mapping(target = "hashFile", ignore = true)
    @Mapping(target = "hashMeta", ignore = true)
    @Mapping(target = "ipfsFile", ignore = true)
    @Mapping(target = "ipfsMeta", ignore = true)
    @Mapping(target = "collection", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "grid", ignore = true)
    public abstract void toEntity(WorkView workView, @MappingTarget Work work);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "publishingDate", ignore = true)
    @Mapping(target = "revocation", ignore = true)
    @Mapping(target = "hashFile", ignore = true)
    @Mapping(target = "hashMeta", ignore = true)
    @Mapping(target = "ipfsFile", ignore = true)
    @Mapping(target = "ipfsMeta", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "filename", ignore = true)
    @Mapping(target = "tags", ignore = true)
    public abstract WorkView toWorkView(WorkCreateView workCreateView);

    @Mapping(source = "workFile.filename", target = "filename")
    @Mapping(source = "status.type", target = "status", defaultValue = "REGISTRATION_CREATED")
    public abstract WorkView toView(Work work);

    @Mapping(source = "workFile.filename", target = "filename")
    public abstract PublicWorkView toPublicView(Work work);

    public abstract WorkFile toWorkFile(WorkFileView workFile);

    public abstract WorkFileView toWorkFileView(WorkFile workFile);

    @Mapping(target = "licenseIdentifier", source = "license.identifier")
    @Mapping(target = "workId", source = "id")
    public abstract MetaDataView toMetaDataView(Work work);

    public abstract IpfsEntryView toIpfsView(IpfsEntry ipfsEntry);

    public abstract HashCodeView toHashView(HashCode hashCode);

    public abstract RevocationView toRevocationView(Revocation revocation);
}
