package eu.fairkom.faircommons.common.models.views;

import java.io.Serializable;

public record HashCodeView(
        String algorithm,
        String hexValue) implements Serializable {
}
