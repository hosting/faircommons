package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.views.IdentifiableView;
import jakarta.persistence.EntityManager;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class EntityObjectFactory {
    private final EntityManager entityManager;

    public EntityObjectFactory(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @ObjectFactory
    public <ID extends Serializable, T> T getOrCreate(@TargetType Class<T> entityClass, IdentifiableView<ID> view) {
        return view.id() != null
                ? entityManager.getReference(entityClass, view.id())
                : BeanUtils.instantiateClass(entityClass);
    }
}
