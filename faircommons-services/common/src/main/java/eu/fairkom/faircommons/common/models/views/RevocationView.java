package eu.fairkom.faircommons.common.models.views;

import java.io.Serializable;
import java.time.ZonedDateTime;

public record RevocationView(
        ZonedDateTime date,
        String note) implements Serializable {
}
