package eu.fairkom.faircommons.common.security.models;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@ConfigurationProperties(ActuatorProperties.PREFIX)
public record ActuatorProperties(
        String username,
        String password) implements Validator {
    public static final String PREFIX = WebSecurityProperties.PREFIX + ".actuator";

    @Override
    public boolean supports(Class<?> clazz) {
        return ActuatorProperties.class == clazz;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Assert.hasText(username, "username may not be empty");
        Assert.hasText(password, "password may not be empty");
    }
}
