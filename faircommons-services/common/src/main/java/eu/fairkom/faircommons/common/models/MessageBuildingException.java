package eu.fairkom.faircommons.common.models;

public class MessageBuildingException extends RuntimeException {
    public MessageBuildingException(String message, Throwable cause) {
        super(message, cause);
    }
}