package eu.fairkom.faircommons.common.models.views;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.fairkom.faircommons.common.models.StorageOption;
import eu.fairkom.faircommons.common.models.WorkType;
import eu.fairkom.faircommons.common.models.WorkVisibility;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public record WorkView(
        UUID id,
        WorkType type,
        String title,
        String description,
        String filename,
        ZonedDateTime registrationDate,
        ZonedDateTime creationDate,
        String status,
        String language,
        String identifier,
        List<String> tags,
        LicenseView license,
        ContributorView creator,
        List<ContributorView> contributors,
        String publisher,
        IpfsEntryView ipfsFile,
        IpfsEntryView ipfsMeta,
        HashCodeView hashFile,
        HashCodeView hashMeta,
        @JsonIgnore Set<StorageOption> storageOptions,
        @JsonIgnore WorkFileView workFile,
        String registrar,
        WorkVisibility visibility,
        RevocationView revocation,
        ZonedDateTime publishingDate,
        String grid) implements IdentifiableView<UUID> {
}
