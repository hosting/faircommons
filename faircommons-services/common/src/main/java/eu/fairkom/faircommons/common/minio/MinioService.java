package eu.fairkom.faircommons.common.minio;

import io.minio.*;
import io.minio.admin.MinioAdminClient;
import io.minio.admin.QuotaUnit;
import io.minio.http.Method;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import static eu.fairkom.faircommons.common.Constants.REGISTRATION_CERTIFICATE_FOLDER;
import static eu.fairkom.faircommons.common.Constants.REGISTRATION_CERTIFICATE_NAME;

@Service
public class MinioService {
    private static final Logger logger = LoggerFactory.getLogger(MinioService.class);
    private final MinioClient minioClient;
    private final MinioAdminClient minioAdminClient;

    private final MinioProperties properties;


    public MinioService(MinioClient minioClient, MinioAdminClient minioAdminClient, MinioProperties properties) {
        this.minioClient = minioClient;
        this.minioAdminClient = minioAdminClient;
        this.properties = properties;
    }

    public String generatePresignedUploadUrl(String userId, String filename) throws MinioException {
        var bucketExists = bucketExists(userId);

        if (!bucketExists) {
            createBucket(userId);
        }

        return getPresignedObjectUrl(userId, filename, Method.PUT);
    }

    public InputStream downloadFile(String userId, String filename) throws MinioException {
        try {
            var downloadFileArgs = GetObjectArgs.builder()
                    .bucket(properties.bucket().prefix() + userId)
                    .object(filename)
                    .build();

            return minioClient.getObject(downloadFileArgs);
        } catch (Exception e) {
            logger.error("Error downloading file for user: {} and file: {}", userId, filename, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }

    public void uploadRegistrationCertificate(String userId, String filenamePrefix, InputStream inputStream) throws MinioException {
        try {
            var filename = filenamePrefix + "/" + REGISTRATION_CERTIFICATE_FOLDER + "/" + REGISTRATION_CERTIFICATE_NAME;
            var uploadFileArgs = PutObjectArgs.builder()
                    .bucket(properties.bucket().prefix() + userId)
                    .object(filename)
                    .stream(inputStream, -1, 5242880)
                    .contentType("application/pdf")
                    .build();

            minioClient.putObject(uploadFileArgs);
        } catch (Exception e) {
            logger.error("Error uploading registration certificate for user: {} with filename prefix: {}", userId, filenamePrefix, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }

    public InputStream downloadRegistrationCertificate(String userId, String filenamePrefix) {
        var filename = filenamePrefix + "/" + REGISTRATION_CERTIFICATE_FOLDER + "/" + REGISTRATION_CERTIFICATE_NAME;
        return downloadFile(userId, filename);
    }

    public void removeFile(String userId, String filename) throws MinioException {
        try {
            var removeFileArgs = RemoveObjectArgs.builder()
                    .bucket(properties.bucket().prefix() + userId)
                    .object(filename)
                    .build();

            minioClient.removeObject(removeFileArgs);
        } catch (Exception e) {
            logger.error("Error removing file for user: {} and file: {}", userId, filename, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }

    private void createBucket(String userId) throws MinioException {
        try {
            minioClient.makeBucket(
                    MakeBucketArgs.builder()
                            .bucket(properties.bucket().prefix() + userId)
                            .build());
            setQuota(properties.bucket().prefix() + userId, properties.bucket().quota(), QuotaUnit.MB);
        } catch (Exception e) {
            logger.error("Error creating bucket for user: {}", userId, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }

    public void setQuota(String bucketName, Long quota, QuotaUnit unit) {
        try {
            minioAdminClient.setBucketQuota(bucketName, quota, unit);
        } catch (Exception e) {
            logger.error("Error setting quota for bucket: {}", bucketName, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }

    private boolean bucketExists(String userId) throws MinioException {
        try {
            return minioClient.bucketExists(BucketExistsArgs.builder()
                    .bucket(properties.bucket().prefix() + userId).build());
        } catch (Exception e) {
            logger.error("Error checking if bucket exists for user: {}", userId, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }

    private String getPresignedObjectUrl(String userId, String filename, Method method) throws MinioException {
        try {
            return minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .bucket(properties.bucket().prefix() + userId)
                            .object(filename)
                            .method(method)
                            .expiry(3, TimeUnit.HOURS)
                            .build());
        } catch (Exception e) {
            logger.error("Error generating presigned object URL for user: {} and file: {}", userId, filename, e);
            throw new MinioException(e.getMessage(), e.getCause());
        }
    }
}
