package eu.fairkom.faircommons.common.rabbitmq;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@EnableRabbit
public class RabbitMqConfiguration {
    public static final String HASHING_QUEUE = "faircommons.hashing-queue";
    public static final String IPFS_QUEUE = "faircommons.ipfs-queue";
    public static final String POST_REGISTRATION_QUEUE = "faircommons.post-registration-queue";
    public static final String POST_REGISTRATION_QUEUE_DLQ = POST_REGISTRATION_QUEUE + ".dlq";
    public static final String STATUS_QUEUE = "faircommons.status-queue";
    public static final String STATUS_QUEUE_DLQ = STATUS_QUEUE + ".dlq";
    public static final String STATUS_EXCHANGE = "status-exchange";
    public static final String STATUS_EXCHANGE_DLX = STATUS_EXCHANGE + ".dlx";
    public static final String POST_REGISTRATION_EXCHANGE = "post-registration-exchange";
    public static final String POST_REGISTRATION_EXCHANGE_DLX = POST_REGISTRATION_EXCHANGE + ".dlx";

    @Bean
    public SimpleMessageConverter messageConverter() {
        var converter = new SimpleMessageConverter();
        var allowedPatterns = List.of("eu.fairkom.faircommons.common.rabbitmq.messages.*",
                "eu.fairkom.faircommons.common.models.*",
                "java.lang.Enum",
                "java.util.*",
                "java.time.*");
        converter.setAllowedListPatterns(allowedPatterns);

        return converter;
    }

    @Bean
    Queue hashingQueue() {
        return QueueBuilder.durable(HASHING_QUEUE)
                .build();
    }

    @Bean
    Queue ipfsQueue() {
        return QueueBuilder.durable(IPFS_QUEUE)
                .build();
    }

    @Bean
    Queue statusQueue() {
        return QueueBuilder.durable(STATUS_QUEUE)
                .withArgument("x-dead-letter-exchange", STATUS_EXCHANGE_DLX)
                .build();
    }

    @Bean
    DirectExchange statusExchange() {
        return new DirectExchange(STATUS_EXCHANGE);
    }

    @Bean
    Binding statusBinding() {
        return BindingBuilder.bind(statusQueue())
                .to(statusExchange()).with(STATUS_QUEUE);
    }

    /**
     * This creates a dead letter queue for the status queue.
     * Any message that cannot be processed in the status listener is moved to this queue.
     */
    @Bean
    Queue statusDeadLetterQueue() {
        return QueueBuilder.durable(STATUS_QUEUE_DLQ)
                .build();
    }

    @Bean
    FanoutExchange statusDeadLetterExchange() {
        return new FanoutExchange(STATUS_EXCHANGE_DLX);
    }

    @Bean
    Binding statusDeadLetterBinding() {
        return BindingBuilder.bind(statusDeadLetterQueue())
                .to(statusDeadLetterExchange());
    }

    @Bean
    Queue postRegistrationQueue() {
        return QueueBuilder.durable(POST_REGISTRATION_QUEUE)
                .withArgument("x-dead-letter-exchange", POST_REGISTRATION_EXCHANGE_DLX)
                .build();
    }

    @Bean
    DirectExchange postRegistrationExchange() {
        return new DirectExchange(POST_REGISTRATION_EXCHANGE);
    }

    @Bean
    Binding postRegistrationBinding() {
        return BindingBuilder.bind(postRegistrationQueue())
                .to(postRegistrationExchange()).with(POST_REGISTRATION_QUEUE);
    }

    @Bean
    Queue postRegistrationDeadLetterQueue() {
        return QueueBuilder.durable(POST_REGISTRATION_QUEUE_DLQ)
                .build();
    }

    @Bean
    FanoutExchange postRegistrationDeadLetterExchange() {
        return new FanoutExchange(POST_REGISTRATION_EXCHANGE_DLX);
    }

    @Bean
    Binding postRegistrationDeadLetterBinding() {
        return BindingBuilder.bind(postRegistrationDeadLetterQueue())
                .to(postRegistrationDeadLetterExchange());
    }
}
