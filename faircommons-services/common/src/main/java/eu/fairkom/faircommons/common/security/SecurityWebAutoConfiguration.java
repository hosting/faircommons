package eu.fairkom.faircommons.common.security;

import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.resource.servlet.OAuth2ResourceServerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration(proxyBeanMethods = false)
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class, SecurityFilterAutoConfiguration.class,
        ManagementWebSecurityAutoConfiguration.class, UserDetailsServiceAutoConfiguration.class,
        OAuth2ResourceServerAutoConfiguration.class
})
@Import(value = {UnsecuredAutoConfigurer.class, ActuatorAutoConfigurer.class, ResourceServerAutoConfigurer.class})
public class SecurityWebAutoConfiguration {
}
