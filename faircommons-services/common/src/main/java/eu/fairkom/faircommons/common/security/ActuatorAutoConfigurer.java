package eu.fairkom.faircommons.common.security;

import eu.fairkom.faircommons.common.security.models.ActuatorProperties;
import eu.fairkom.faircommons.common.security.models.ConfigurerOrders;
import eu.fairkom.faircommons.common.security.models.WebSecurityProperties;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(prefix = WebSecurityProperties.PREFIX, name = "enabled", matchIfMissing = true)
@ConditionalOnWebApplication
@EnableConfigurationProperties(value = {ActuatorProperties.class})
@EnableWebSecurity
public class ActuatorAutoConfigurer {
    private final ActuatorProperties actuatorProperties;

    public ActuatorAutoConfigurer(ActuatorProperties actuatorProperties) {
        this.actuatorProperties = actuatorProperties;
    }

    @Bean
    @Order(ConfigurerOrders.ACTUATOR)
    public SecurityFilterChain actuatorFilterChain(HttpSecurity http) throws Exception {
        http.securityMatcher(EndpointRequest.toAnyEndpoint())
                .userDetailsService(userDetailsService())
                .cors(withDefaults())
                .csrf(CsrfConfigurer::disable)
                .httpBasic(withDefaults())
                .sessionManagement(SessionManagementConfigurer::disable)
                .authorizeHttpRequests(authorize ->
                        authorize.anyRequest().authenticated());


        return http.build();
    }

    public UserDetailsService userDetailsService() {
        var encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        var user = User.withUsername(actuatorProperties.username())
                .password(encoder.encode(actuatorProperties.password()))
                .authorities(List.of())
                .build();

        return new InMemoryUserDetailsManager(user);
    }
}
