package eu.fairkom.faircommons.common.rabbitmq.messages;

import java.io.Serializable;
import java.util.UUID;

public interface Message extends Serializable {
    MessageType type();

    UUID workId();
}
