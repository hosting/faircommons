package eu.fairkom.faircommons.common.models.views;

public record PublicContributorView(
        PublicAuthorView author,
        Integer percentage) {
}