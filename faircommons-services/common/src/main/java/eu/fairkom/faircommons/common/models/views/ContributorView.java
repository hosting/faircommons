package eu.fairkom.faircommons.common.models.views;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record ContributorView(
        UUID id,
        @Valid AuthorView author,
        @NotNull Integer percentage) implements IdentifiableView<UUID> {

}