package eu.fairkom.faircommons.common.minio;

import io.minio.MinioClient;
import io.minio.admin.MinioAdminClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(MinioClient.class)
@EnableConfigurationProperties(MinioProperties.class)
public class MinioConfiguration {
    private final MinioProperties properties;

    public MinioConfiguration(MinioProperties minioProperties) {
        this.properties = minioProperties;
    }

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(properties.url())
                .region(properties.region())
                .credentials(properties.accessKey(), properties.secretKey())
                .build();
    }

    @Bean
    public MinioAdminClient minioAdminClient() {
        return MinioAdminClient.builder()
                .endpoint(properties.url())
                .region(properties.region())
                .credentials(properties.accessKey(), properties.secretKey())
                .build();
    }
}
