package eu.fairkom.faircommons.common.rabbitmq.messages;

import eu.fairkom.faircommons.common.models.views.WorkStatusView;

import java.util.UUID;

public record StatusEvent(
        UUID workId,
        WorkStatusView status,
        MessageType type) implements Message {

    public StatusEvent(UUID workId, WorkStatusView status) {
        this(workId, status, MessageType.SUCCESS);
    }
}
