package eu.fairkom.faircommons.common.security.models;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.DefaultValue;

import java.util.List;

@ConfigurationProperties(UnsecuredProperties.PREFIX)
public record UnsecuredProperties(
        @DefaultValue({"/swagger-ui.html",
                "/swagger-ui/**",
                "/v3/api-docs/**",
                "/actuator/health"}) List<String> paths) {
    public static final String PREFIX = WebSecurityProperties.PREFIX + ".unsecured";
}
