package eu.fairkom.faircommons.common.models.views;

import eu.fairkom.faircommons.common.models.entities.WorkStatus;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

public record WorkStatusView(
        UUID workId,
        ZonedDateTime created,
        HashCodeView hash,
        IpfsEntryView ipfsEntry,
        String errorMessage,
        WorkStatus.WorkStatusType type) implements Serializable {

    public WorkStatusView(UUID workId, WorkStatus.WorkStatusType type) {
        this(workId, null, null, null, null, type);
    }

    public WorkStatusView(UUID workId, HashCodeView hash, WorkStatus.WorkStatusType type) {
        this(workId, null, hash, null, null, type);
    }

    public WorkStatusView(UUID workId, IpfsEntryView ipfsEntry, WorkStatus.WorkStatusType type) {
        this(workId, null, null, ipfsEntry, null, type);
    }

    public WorkStatusView(UUID workId, String errorMessage, WorkStatus.WorkStatusType type) {
        this(workId, null, null, null, errorMessage, type);
    }
}
