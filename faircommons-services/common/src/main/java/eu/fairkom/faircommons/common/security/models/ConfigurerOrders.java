package eu.fairkom.faircommons.common.security.models;

public final class ConfigurerOrders {

    public static final int UNSECURED = 0;
    public static final int ACTUATOR = UNSECURED + 1;
    public static final int RESOURCE = ACTUATOR + 1;
    private ConfigurerOrders() {
    }
}