package eu.fairkom.faircommons.common.models.views;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.UUID;

public record AuthorView(
        UUID id,
        @Size(max = 255) @NotBlank String firstName,
        @Size(max = 255) @NotBlank String lastName,
        @Size(max = 255) @Email String email,
        @Size(max = 255) @NotBlank String pseudonym,
        @Size(max = 255) String url) implements IdentifiableView<UUID> {
}