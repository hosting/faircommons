package eu.fairkom.faircommons.common.models.entities.embedded;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class IpfsEntry {
    @Column(updatable = false)
    private String cid;

    @Column(updatable = false)
    private String path;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
