package eu.fairkom.faircommons.common.models.entities;

import eu.fairkom.faircommons.common.models.entities.embedded.HashCode;
import eu.fairkom.faircommons.common.models.entities.embedded.IpfsEntry;
import jakarta.persistence.*;
import org.springframework.data.annotation.Immutable;

import java.time.ZonedDateTime;

@Entity
@Immutable
public class WorkStatus extends Identifiable<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Work work;

    private ZonedDateTime created = ZonedDateTime.now();

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "algorithm", column = @Column(name = "hash_algorithm")),
            @AttributeOverride(name = "hexValue", column = @Column(name = "hash_hex_value"))
    })
    private HashCode hashCode;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "cid", column = @Column(name = "ipfs_cid")),
            @AttributeOverride(name = "path", column = @Column(name = "ipfs_path"))
    })
    private IpfsEntry ipfsEntry;

    private WorkStatusType type;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(Work work) {
        this.work = work;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public HashCode getHashCode() {
        return hashCode;
    }

    public void setHashCode(HashCode hashCode) {
        this.hashCode = hashCode;
    }

    public IpfsEntry getIpfsEntry() {
        return ipfsEntry;
    }

    public void setIpfsEntry(IpfsEntry ipfsEntry) {
        this.ipfsEntry = ipfsEntry;
    }

    public WorkStatusType getType() {
        return type;
    }

    public void setType(WorkStatusType type) {
        this.type = type;
    }

    public enum WorkStatusType {
        FILE_HASHING_DONE, META_HASHING_DONE, FILE_IPFS_DONE, META_IPFS_DONE, REGISTRATION_DONE, ERROR
    }
}
