package eu.fairkom.faircommons.common.mappers;

import eu.fairkom.faircommons.common.models.entities.Tag;
import eu.fairkom.faircommons.common.models.views.TagView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MappingConfig.class)
public interface TagMapper {
    TagView toView(Tag tag);

    Tag toEntity(TagView tag);

    @Mapping(target = "name")
    @Mapping(target = "id", ignore = true)
    Tag toEntity(String name);


    default String toTagName(Tag tag) {
        return tag.getName();
    }
}
