package eu.fairkom.faircommons.common.models;

public enum WorkVisibility {
    PUBLIC, PRIVATE
}
