package eu.fairkom.faircommons.common.models.views;

import java.io.Serializable;

public record IpfsEntryView(
        String cid,
        String path) implements Serializable {

    public IpfsEntryView(String cid) {
        this(cid, "");
    }
}